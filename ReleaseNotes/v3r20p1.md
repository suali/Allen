2023-09-27 Allen v3r20p1
===

This version uses
Rec [v35r19](../../../../Rec/-/tags/v35r19),
Lbcom [v34r18](../../../../Lbcom/-/tags/v34r18),
LHCb [v54r18](../../../../LHCb/-/tags/v54r18),
Detector [v1r21](../../../../Detector/-/tags/v1r21),
Gaudi [v36r16](../../../../Gaudi/-/tags/v36r16) and
LCG [103](http://lcginfo.cern.ch/release/103/) with ROOT 6.28.00.

This version is released on the `master` branch.
Built relative to Allen [v3r20](/../../tags/v3r20), with the following changes:

### New features ~"new feature"



### Fixes ~"bug fix" ~workaround

- ~Luminosity | Lumi counters: loop only over real velo clusters, !1343 (@dcraik)


### Enhancements ~enhancement



### Code cleanups and changes to tests ~modernisation ~cleanup ~testing



### Documentation ~Documentation


### Other

