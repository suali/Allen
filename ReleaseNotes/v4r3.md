2024-03-18 Allen v4r3
===

This version uses
Rec [v36r3](../../../../Rec/-/tags/v36r3),
Lbcom [v35r3](../../../../Lbcom/-/tags/v35r3),
LHCb [v55r3](../../../../LHCb/-/tags/v55r3),
Gaudi [v38r1](../../../../Gaudi/-/tags/v38r1),
Detector [v1r28](../../../../Detector/-/tags/v1r28) and
LCG [105a](http://lcginfo.cern.ch/release/105a/) with ROOT 6.30.04.

This version is released on the `master` branch.
Built relative to Allen [v4r2](/../../tags/v4r2), with the following changes:

### New features ~"new feature"



### Fixes ~"bug fix" ~workaround

- Remove aggregate in CalcMaxCombos, !1494 (@thboettc) [#519]
- Fix mismatch between event lists by getting rid of mask input in algorithm with input aggregates, !1489 (@acasaisv)
- Fix TAE on master, !1486 (@cagapopo)
- Fix FilterVeloTracks crash by limiting the maximum number of interaction seeds, !1485 (@samarian)
- Speed up error banks line, !1477 (@raaij)
- Fix algorithm and lines init function not being called, !1472 (@ahennequ)


### Enhancements ~enhancement

- Optimise run_lines, !1456 (@ahennequ)
- Merging HLT1 NN ghost killer with forward_then_matching_no_ut optimisation, !1442 (@ascarabo)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- Enable SMOG2 flag in all sequences, !1491 (@samarian)
- First step for https://gitlab.cern.ch/lhcb/Allen/-/issues/518 -> remove partial throughput tests, !1490 (@samarian)
- PV reconstruction : Homogenizing Allen and Moore PV finders. Change of rho cut and Kalman Filter covariance matrix initialisation, !1459 (@bokutsen)


### Documentation ~Documentation


### Other

- ~selections | BWDIV: infrastructure to put in tuned lines, !1431 (@tevans)
- ~PID | ElectronID NN and MVA machinery refactor, !1398 (@acasaisv)
- ~Monitoring | Update physics and smog routing bits for pp monitoring, !1469 (@tmombach)
- Enable BGIPseudoPV lines for hlt1_pp_* sequences, !1463 (@kaaricha)
- Modify thresholds for D2KPi alignment line, !1430 (@gtuci)
- Add D* -> D0 (-> K pi) pi container and lines, !1423 (@thboettc)
- Hotfix Muon Event Model, !1418 (@acasaisv)
- Loose IP cut for dihadrons., !1389 (@thboettc)
