2023-10-06 Allen v3r21
===

This version uses
Rec [v35r20](../../../../Rec/-/tags/v35r20),
Lbcom [v34r19](../../../../Lbcom/-/tags/v34r19),
LHCb [v54r19](../../../../LHCb/-/tags/v54r19),
Gaudi [v36r16](../../../../Gaudi/-/tags/v36r16),
Detector [v1r22](../../../../Detector/-/tags/v1r22) and
LCG [103](http://lcginfo.cern.ch/release/103/) with ROOT 6.28.00.

This version is released on the `master` branch.
Built relative to Allen [v3r20p1](/../../tags/v3r20p1), with the following changes:

### New features ~"new feature"

- ~Calo | Added photon lowmult lines in the ion sequence, !1254 (@rangel)
- DiPhoton UPC line, !1352 (@rangel)


### Fixes ~"bug fix" ~workaround

- ~Composites | Fix max SVs and protect against too many candidates, !1350 (@thboettc)
- ~Monitoring | Align PbPb reconstruction algorithm names to pp, !1351 (@cagapopo)


### Enhancements ~enhancement

- ~Monitoring | Added Hlt1HeavyIonPbPbUPCMB to SciFi alignment routing bit, !1354 (@cagapopo)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- Fix CI job for formatting, !1348 (@rmatev)
- PbPb BGI lines and (disabled) particle based lines for alignment+monitoring, !1345 (@cagapopo)
- Adapted to drop of GaudiAlgorithm in FT, !1291 (@sponce)


### Documentation ~Documentation


### Other

- Fix errors caused by DD4Hep future upgrades refactor, !1321 (@emmuhamm)
