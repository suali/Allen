/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration          *
*                                                                             *
* This software is distributed under the terms of the Apache License          *
* version 2 (Apache-2.0), copied verbatim in the file "LICENSE".              *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "States.cuh"
#include "AlgorithmTypes.cuh"
#include "VeloConsolidated.cuh"

namespace CountMaterialInteractionCandidates {

  struct Parameters {
    HOST_INPUT(host_number_of_events_t, unsigned) host_number_of_events;
    HOST_INPUT(host_number_of_reconstructed_velo_tracks_t, unsigned) host_number_of_reconstructed_velo_tracks;

    MASK_INPUT(dev_event_list_t) dev_event_list;

    DEVICE_INPUT(dev_number_of_events_t, unsigned) dev_number_of_events;
    DEVICE_INPUT(dev_velo_tracks_view_t, Allen::Views::Velo::Consolidated::Tracks) dev_velo_track_view;
    DEVICE_INPUT(dev_velo_states_view_t, Allen::Views::Physics::KalmanStates) dev_velo_states_view;
    DEVICE_OUTPUT(dev_filtered_velo_track_idx_t, unsigned) dev_filtered_velo_track_idx;
    DEVICE_OUTPUT(dev_number_of_filtered_tracks_t, unsigned) dev_number_of_filtered_tracks;
    DEVICE_OUTPUT(dev_number_of_seeds_t, unsigned) dev_number_of_seeds;

    PROPERTY(beamdoca_r_t, "beamdoca_r", "radial doca to the beamspot", float) beamdoca_r;
    PROPERTY(
      max_doca_for_close_track_pairs_t,
      "max_doca_for_close_track_pairs",
      "doca to define close track pairs",
      float)
    max_doca_for_close_track_pairs;

    PROPERTY(block_dim_t, "block_dim", "block dimension", DeviceDimensions) block_dim;
  };

  __global__ void count_materialinteraction_candidates(Parameters, float*);

  struct count_materialinteraction_candidates_t : public DeviceAlgorithm, Parameters {
    void set_arguments_size(ArgumentReferences<Parameters> arguments, const RuntimeOptions&, const Constants&) const;

    void operator()(
      const ArgumentReferences<Parameters>& arguments,
      const RuntimeOptions&,
      const Constants&,
      const Allen::Context& context) const;

  private:
    Property<beamdoca_r_t> m_beamdoca_r {this, 3.5f};
    Property<max_doca_for_close_track_pairs_t> m_max_doca_for_close_track_pairs {this, 0.15f};
    Property<block_dim_t> m_block_dim {this, {{256, 1, 1}}};
  };

} // namespace CountMaterialInteractionCandidates
