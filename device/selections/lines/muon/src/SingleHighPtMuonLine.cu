/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the Apache License          *
* version 2 (Apache-2.0), copied verbatim in the file "LICENSE".              *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "SingleHighPtMuonLine.cuh"

// Explicit instantiation
INSTANTIATE_LINE(single_high_pt_muon_line::single_high_pt_muon_line_t, single_high_pt_muon_line::Parameters)

__device__ bool single_high_pt_muon_line::single_high_pt_muon_line_t::select(
  const Parameters& parameters,
  std::tuple<const Allen::Views::Physics::BasicParticle> input)
{
  const auto track = std::get<0>(input);
  const bool decision = track.state().chi2() / track.state().ndof() < parameters.maxChi2Ndof &&
                        track.state().pt() > parameters.singleMinPt && track.state().p() > parameters.singleMinP &&
                        track.is_muon() && track.state().z() > parameters.minZ;

  return decision;
}

__device__ void single_high_pt_muon_line::single_high_pt_muon_line_t::fill_tuples(
  const Parameters& parameters,
  std::tuple<const Allen::Views::Physics::BasicParticle> input,
  unsigned index,
  bool sel)
{
  if (sel) parameters.pt[index] = std::get<0>(input).state().pt();
}
