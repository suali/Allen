/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the Apache License          *
* version 2 (Apache-2.0), copied verbatim in the file "LICENSE".              *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "AlgorithmTypes.cuh"
#include "CompositeParticleLine.cuh"
#include "ROOTService.h"
#include "MassDefinitions.h"

#include "AllenMonitoring.h"

namespace d2kpi_line {
  struct Parameters {
    HOST_INPUT(host_number_of_events_t, unsigned) host_number_of_events;
    HOST_INPUT(host_number_of_svs_t, unsigned) host_number_of_svs;
    DEVICE_INPUT(dev_particle_container_t, Allen::Views::Physics::MultiEventCompositeParticles) dev_particle_container;
    MASK_INPUT(dev_event_list_t) dev_event_list;
    HOST_OUTPUT(host_line_data_t, LineData) host_line_data;
    HOST_OUTPUT_WITH_DEPENDENCIES(host_fn_parameters_t, DEPENDENCIES(dev_particle_container_t), char)
    host_fn_parameters;

    // Monitoring
    DEVICE_OUTPUT(min_pt_t, float) min_pt; // To be used in bandwidth division
    DEVICE_OUTPUT(min_ip_t, float) min_ip; // To be used in bandwidth division
    DEVICE_OUTPUT(D0_ct_t, float) D0_ct;   // To be used in bandwidth division
    DEVICE_OUTPUT(evtNo_t, uint64_t) evtNo;
    DEVICE_OUTPUT(runNo_t, unsigned) runNo;

    PROPERTY(pre_scaler_t, "pre_scaler", "Pre-scaling factor", float) pre_scaler;
    PROPERTY(post_scaler_t, "post_scaler", "Post-scaling factor", float) post_scaler;
    PROPERTY(pre_scaler_hash_string_t, "pre_scaler_hash_string", "Pre-scaling hash string", std::string);
    PROPERTY(post_scaler_hash_string_t, "post_scaler_hash_string", "Post-scaling hash string", std::string);
    PROPERTY(minComboPt_t, "minComboPt", "minComboPt description", float) minComboPt;
    PROPERTY(maxVertexChi2_t, "maxVertexChi2", "maxVertexChi2 description", float) maxVertexChi2;
    PROPERTY(maxDOCA_t, "maxDOCA", "maxDOCA description", float) maxDOCA;
    PROPERTY(minEta_t, "minEta", "minEta description", float) minEta;
    PROPERTY(maxEta_t, "maxEta", "maxEta description", float) maxEta;
    PROPERTY(minTrackPt_t, "minTrackPt", "minTrackPt description", float) minTrackPt;
    PROPERTY(minTrackP_t, "minTrackP", "minTrackP description", float) minTrackP;
    PROPERTY(massWindow_t, "massWindow", "massWindow description", float) massWindow;
    PROPERTY(minTrackIP_t, "minTrackIP", "minTrackIP description", float) minTrackIP;
    PROPERTY(ctIPScale_t, "ctIPScale", "D0 ct should be larger than this time minTrackIP", float) ctIPScale;
    PROPERTY(minZ_t, "minZ", "minimum vertex z coordinate", float) minZ;
    PROPERTY(minDira_t, "minDira", "minimum value of cos(theta_dira)", float) minDira;
    PROPERTY(OppositeSign_t, "OppositeSign", "Selects opposite sign dibody combinations", bool) OppositeSign;
    PROPERTY(enable_monitoring_t, "enable_monitoring", "Enable line monitoring", bool) enable_monitoring;
    PROPERTY(enable_tupling_t, "enable_tupling", "Enable line tupling", bool) enable_tupling;
  };

  struct d2kpi_line_t : public SelectionAlgorithm, Parameters, CompositeParticleLine<d2kpi_line_t, Parameters> {
    struct DeviceAccumulators {
      Allen::Monitoring::Histogram<>::DeviceType histogram_d0_mass;
      Allen::Monitoring::Histogram<>::DeviceType histogram_d0_pt;
      Allen::Monitoring::Histogram<>::DeviceType histogram_p0_ipx;
      Allen::Monitoring::Histogram<>::DeviceType histogram_p0_ipy;
      Allen::Monitoring::Histogram<>::DeviceType histogram_p1_ipx;
      Allen::Monitoring::Histogram<>::DeviceType histogram_p1_ipy;
      DeviceAccumulators(const d2kpi_line_t& algo, const Allen::Context& ctx) :
        histogram_d0_mass(algo.m_histogram_d0_mass.data(ctx)), histogram_d0_pt(algo.m_histogram_d0_pt.data(ctx)),
        histogram_p0_ipx(algo.m_histogram_p0_ipx.data(ctx)), histogram_p0_ipy(algo.m_histogram_p0_ipy.data(ctx)),
        histogram_p1_ipx(algo.m_histogram_p1_ipx.data(ctx)), histogram_p1_ipy(algo.m_histogram_p1_ipy.data(ctx))
      {}
    };

    __device__ static bool
    select(const Parameters&, const DeviceAccumulators&, std::tuple<const Allen::Views::Physics::CompositeParticle>);

    __device__ static void fill_tuples(
      const Parameters& parameters,
      std::tuple<const Allen::Views::Physics::CompositeParticle> input,
      unsigned index,
      bool sel);
    __device__ static void monitor(
      const Parameters& parameters,
      const DeviceAccumulators& accumulators,
      std::tuple<const Allen::Views::Physics::CompositeParticle> input,
      unsigned index,
      bool sel);

    using monitoring_types = std::tuple<min_pt_t, min_ip_t, D0_ct_t, evtNo_t, runNo_t>;

  private:
    Property<pre_scaler_t> m_pre_scaler {this, 1.f};
    Property<post_scaler_t> m_post_scaler {this, 1.f};
    Property<pre_scaler_hash_string_t> m_pre_scaler_hash_string {this, ""};
    Property<post_scaler_hash_string_t> m_post_scaler_hash_string {this, ""};
    Property<minComboPt_t> m_minComboPt {this, 2000.0f * Gaudi::Units::MeV};
    Property<maxVertexChi2_t> m_maxVertexChi2 {this, 20.f};
    Property<maxDOCA_t> m_maxDOCA {this, 0.2f * Gaudi::Units::mm};
    Property<minEta_t> m_minEta {this, 2.0f};
    Property<maxEta_t> m_maxEta {this, 5.0f};
    Property<minTrackPt_t> m_minTrackPt {this, 800.f * Gaudi::Units::MeV};
    Property<minTrackP_t> m_minTrackP {this, 0.f * Gaudi::Units::MeV};
    Property<massWindow_t> m_massWindow {this, 100.f * Gaudi::Units::MeV};
    Property<minTrackIP_t> m_minTrackIP {this, 0.06f * Gaudi::Units::mm};
    Property<ctIPScale_t> m_ctIPScale {this, 1.f};
    Property<minZ_t> m_minZ {this, -341.f * Gaudi::Units::mm};
    Property<minDira_t> m_minDira {this, 0.f};
    Property<OppositeSign_t> m_opposite_sign {this, true};
    Property<enable_monitoring_t> m_enable_monitoring {this, false};
    Property<enable_tupling_t> m_enable_tupling {this, false};

    Allen::Monitoring::Histogram<> m_histogram_d0_mass {this, "d0_mass", "m(D0)", {100u, 1765.f, 1965.f}};
    Allen::Monitoring::Histogram<> m_histogram_d0_pt {this, "d0_pt", "pT(D0)", {100u, 800.f, 1e4f}};
    Allen::Monitoring::Histogram<> m_histogram_p0_ipx {this, "p0_ipx", "IP_{x}(p0)", {100u, -3.f, 3.f}};
    Allen::Monitoring::Histogram<> m_histogram_p0_ipy {this, "p0_ipy", "IP_{y}(p0)", {100u, -3.f, 3.f}};
    Allen::Monitoring::Histogram<> m_histogram_p1_ipx {this, "p1_ipx", "IP_{x}(p1)", {100u, -3.f, 3.f}};
    Allen::Monitoring::Histogram<> m_histogram_p1_ipy {this, "p1_ipy", "IP_{y}(p1)", {100u, -3.f, 3.f}};
  };
} // namespace d2kpi_line
