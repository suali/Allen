/*****************************************************************************\
* (c) Copyright 2018-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the Apache License          *
* version 2 (Apache-2.0), copied verbatim in the file "LICENSE".              *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

namespace Allen {
  namespace Units {
    //
    // Data size
    //
    constexpr unsigned kB = 1024;
    constexpr unsigned MB = kB * kB;
    constexpr unsigned GB = MB * kB;
  } // namespace Units
} // namespace Allen
