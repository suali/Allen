###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the Apache License          #
# version 2 (Apache-2.0), copied verbatim in the file "COPYING".              #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from AllenConf.thresholds.thresholds import Thresholds

threshold_settings = Thresholds(
    TrackMuonMVA_maxCorrChi2=1.8,
    DiMuonHighMass_maxCorrChi2=1.8,
    DiMuonDisplaced_maxCorrChi2=1.8,
    D2HH_ctIPScale=1.,
    TrackMVA_alpha=1300,
    TrackElectronMVA_alpha=2900,
    TrackMuonMVA_alpha=-300,
    D2HH_track_ip=0.07,
    D2HH_track_pt=800,
    SingleHighPtLepton_pt=5000,
    SingleHighPtLepton_pt_noMuonID=5000,
    TwoTrackMVA_minMVA=0.975,
    TwoTrackKs_minTrackPt_piKs=762.889,
    TwoTrackKs_minTrackIPChi2_piKs=76.6865,
    TwoTrackKs_minComboPt_Ks=2500,
    TwoTrackKs_maxEta_Ks=4.2,
    TwoTrackKs_min_combip=1.38,
    DiMuonHighMass_pt=700,
    DiElectronDisplaced_pt=1000,
    DiElectronDisplaced_ipchi2=8.2,
    DiMuonDisplaced_pt=500,
    DiMuonDisplaced_ipchi2=6,
    DiPhotonHighMass_minET=4200,
    TrackMVA_maxGhostProb=0.1,
    TwoTrackMVA_maxGhostProb=0.1,
    LambdaLLDetachedTrack_track_mipchi2=9.5,
    LambdaLLDetachedTrack_combination_bpvfd=50,
    XiOmegaLLL_track_ipchi2=41.5)
