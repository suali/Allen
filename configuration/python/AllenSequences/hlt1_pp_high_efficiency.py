###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the Apache License          #
# version 2 (Apache-2.0), copied verbatim in the file "LICENSE".              #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from AllenConf.HLT1 import setup_hlt1_node
from AllenConf.ut_reconstruction import make_ut_tracks
from AllenCore.generator import generate

with make_ut_tracks.bind(restricted=False):
    hlt1_node = setup_hlt1_node(enableRateValidator=True)

generate(hlt1_node)
