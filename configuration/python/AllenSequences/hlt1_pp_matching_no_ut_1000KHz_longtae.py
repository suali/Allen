###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the Apache License          #
# version 2 (Apache-2.0), copied verbatim in the file "LICENSE".              #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from AllenConf.HLT1 import setup_hlt1_node
from AllenCore.generator import generate
from AllenConf.enum_types import TrackingType
from AllenConf.get_thresholds import get_thresholds
from AllenConf.utils import make_tae_activity_filter

with make_tae_activity_filter.bind(
        use_long_tracks=True, name="tae_long_activity_filter"):
    hlt1_node = setup_hlt1_node(
        tracking_type=TrackingType.MATCHING,
        threshold_settings=get_thresholds("no_ut_tuned_1000KHz"),
        with_ut=False,
        tae_activity=True)
generate(hlt1_node)
