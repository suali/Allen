long_validator validation:
TrackChecker output                               :      5243/    69373   7.56% ghosts
for P>3GeV,Pt>0.5GeV                              :      1697/    35869   4.73% ghosts
01_long                                           :     58899/    78155  75.36% ( 76.36%),      1062 (  1.77%) clones, pur  99.05%, hit eff  97.81%
02_long_P>5GeV                                    :     46251/    51059  90.58% ( 91.47%),       854 (  1.81%) clones, pur  99.11%, hit eff  98.12%
03_long_strange                                   :      2389/     3858  61.92% ( 62.97%),        35 (  1.44%) clones, pur  99.03%, hit eff  97.54%
04_long_strange_P>5GeV                            :      1569/     1834  85.55% ( 86.85%),        22 (  1.38%) clones, pur  99.04%, hit eff  97.90%
05_long_fromB                                     :      3994/     4585  87.11% ( 87.82%),        72 (  1.77%) clones, pur  98.99%, hit eff  98.39%
06_long_fromB_P>5GeV                              :      3567/     3752  95.07% ( 95.31%),        69 (  1.90%) clones, pur  99.04%, hit eff  98.59%
07_long_electrons                                 :      2188/     5658  38.67% ( 39.18%),        62 (  2.76%) clones, pur  98.53%, hit eff  97.38%
08_long_electrons_P>5GeV                          :      1681/     2953  56.93% ( 58.25%),        54 (  3.11%) clones, pur  98.39%, hit eff  97.39%
09_long_fromB_electrons                           :       133/      240  55.42% ( 59.34%),         2 (  1.48%) clones, pur  98.93%, hit eff  98.17%
10_long_fromB_electrons_P>5GeV                    :       120/      168  71.43% ( 75.48%),         1 (  0.83%) clones, pur  98.86%, hit eff  98.30%
long_P>5GeV_AND_Pt>1GeV                           :     12653/    13336  94.88% ( 95.58%),       202 (  1.57%) clones, pur  98.94%, hit eff  98.45%
long_fromB_P>5GeV_AND_Pt>1GeV                     :      2408/     2500  96.32% ( 96.40%),        44 (  1.79%) clones, pur  98.97%, hit eff  98.76%
11_noVelo_UT                                      :         0/     8699   0.00% (  0.00%),         0 (  0.00%) clones, pur   -nan%, hit eff   -nan%
12_noVelo_UT_P>5GeV                               :         0/     3556   0.00% (  0.00%),         0 (  0.00%) clones, pur   -nan%, hit eff   -nan%
13_long_PT>2GeV                                   :      3145/     3268  96.24% ( 96.39%),        46 (  1.44%) clones, pur  98.95%, hit eff  98.76%
14_long_from_B_PT>2GeV                            :      1128/     1172  96.25% ( 96.31%),        10 (  0.88%) clones, pur  98.93%, hit eff  99.04%
15_long_strange_P>5GeV                            :      1569/     1834  85.55% ( 86.85%),        22 (  1.38%) clones, pur  99.04%, hit eff  97.90%
16_long_strange_P>5GeV_PT>500MeV                  :       679/      738  92.01% ( 91.95%),         6 (  0.88%) clones, pur  98.86%, hit eff  98.41%
17_long_fromSignal                                :      2430/     2621  92.71% ( 92.79%),        47 (  1.90%) clones, pur  98.94%, hit eff  98.56%


muon_validator validation:
Muon fraction in all MCPs:                                              14960/  1181372   0.01% 
Muon fraction in MCPs to which a track(s) was matched:                    782/    73998   0.01% 
Correctly identified muons with isMuon:                                   674/      782  86.19% 
Correctly identified muons from strange decays with isMuon:                 2/        2 100.00% 
Correctly identified muons from B decays with isMuon:                     123/      134  91.79% 
Tracks identified as muon with isMuon, but matched to non-muon MCP:      8213/    73216  11.22% 
Ghost tracks identified as muon with isMuon:                              900/     5243  17.17% 


pv_validator validation:
REC and MC vertices matched by dz distance
MC PV is reconstructible if at least 4 tracks are reconstructed
MC PV is isolated if dz to closest reconstructible MC PV > 10.00 mm
REC and MC vertices matched by dz distance

All                  :  0.942 ( 16857/ 17901)
Isolated             :  0.979 (  8730/  8916)
Close                :  0.905 (  8127/  8985)
False rate           :  0.019 (   321/ 17178)
Real false rate      :  0.019 (   321/ 17178)
Clones               :  0.000 (     0/ 16857)


rate_validator validation:
Hlt1TrackMVA:                                     251/  1000, ( 7530.00 +/-   411.34) kHz
Hlt1TwoTrackMVA:                                  598/  1000, (17940.00 +/-   465.14) kHz
Hlt1D2KK:                                          31/  1000, (  930.00 +/-   164.42) kHz
Hlt1D2KPi:                                         39/  1000, ( 1170.00 +/-   183.66) kHz
Hlt1D2PiPi:                                        23/  1000, (  690.00 +/-   142.21) kHz
Hlt1Dst2D0Pi:                                       2/  1000, (   60.00 +/-    42.38) kHz
Hlt1KsToPiPi:                                       0/  1000, (    0.00 +/-     0.00) kHz
Hlt1KsToPiPiDoubleMuonMisID:                        0/  1000, (    0.00 +/-     0.00) kHz
Hlt1TwoTrackKs:                                     8/  1000, (  240.00 +/-    84.51) kHz
Hlt1TwoKs:                                          2/  1000, (   60.00 +/-    42.38) kHz
Hlt1LambdaLLDetachedTrack:                          1/  1000, (   30.00 +/-    29.98) kHz
Hlt1XiOmegaLLL:                                     0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SingleHighPtMuon:                               3/  1000, (   90.00 +/-    51.88) kHz
Hlt1SingleHighPtMuonNoMuID:                         9/  1000, (  270.00 +/-    89.59) kHz
Hlt1DiMuonHighMass:                                19/  1000, (  570.00 +/-   129.52) kHz
Hlt1DiMuonDisplaced:                               32/  1000, (  960.00 +/-   166.97) kHz
Hlt1DiMuonSoft:                                     3/  1000, (   90.00 +/-    51.88) kHz
Hlt1TrackMuonMVA:                                  17/  1000, (  510.00 +/-   122.64) kHz
Hlt1DiMuonNoIP:                                     5/  1000, (  150.00 +/-    66.91) kHz
Hlt1DiMuonNoIP_SS:                                  0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDrellYan_VLowMass:                        0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDrellYan_VLowMass_SS:                     0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDrellYan:                                 0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDrellYan_SS:                              0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DetJpsiToMuMuPosTagLine:                        1/  1000, (   30.00 +/-    29.98) kHz
Hlt1DetJpsiToMuMuNegTagLine:                        1/  1000, (   30.00 +/-    29.98) kHz
Hlt1TrackElectronMVA:                              56/  1000, ( 1680.00 +/-   218.12) kHz
Hlt1SingleHighPtElectron:                           8/  1000, (  240.00 +/-    84.51) kHz
Hlt1DiElectronDisplaced:                           17/  1000, (  510.00 +/-   122.64) kHz
Hlt1DiPhotonHighMass:                              13/  1000, (  390.00 +/-   107.46) kHz
Hlt1Pi02GammaGamma:                                 0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronHighMass_SS:                          1/  1000, (   30.00 +/-    29.98) kHz
Hlt1DiElectronHighMass:                             1/  1000, (   30.00 +/-    29.98) kHz
Hlt1DiElectronLowMass_massSlice1_prompt:            1/  1000, (   30.00 +/-    29.98) kHz
Hlt1DiElectronLowMass_SS_massSlice1_prompt:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice2_prompt:            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice2_prompt:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice3_prompt:            1/  1000, (   30.00 +/-    29.98) kHz
Hlt1DiElectronLowMass_SS_massSlice3_prompt:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice4_prompt:            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice4_prompt:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice1_displaced:         1/  1000, (   30.00 +/-    29.98) kHz
Hlt1DiElectronLowMass_SS_massSlice1_displaced:      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice2_displaced:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice2_displaced:      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice3_displaced:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice3_displaced:      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice4_displaced:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice4_displaced:      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1Passthrough:                                    0/  1000, (    0.00 +/-     0.00) kHz
Hlt1TAEPassthrough:                                 0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsNoBeam:                             0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsBeamOne:                            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsBeamTwo:                            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsUpBeamBeam:                         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsDownBeamBeam:                       0/  1000, (    0.00 +/-     0.00) kHz
Hlt1ODINLumi:                                       0/  1000, (    0.00 +/-     0.00) kHz
Hlt1ODIN1kHzLumi:                                   0/  1000, (    0.00 +/-     0.00) kHz
Hlt1ODINCalib:                                      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1ErrorBank:                                      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1VeloMicroBiasVeloClosing:                       0/  1000, (    0.00 +/-     0.00) kHz
Hlt1RICH1Alignment:                                 2/  1000, (   60.00 +/-    42.38) kHz
Hlt1RICH2Alignment:                                 0/  1000, (    0.00 +/-     0.00) kHz
Hlt1D2KPiAlignment:                                12/  1000, (  360.00 +/-   103.30) kHz
Hlt1Dst2D0PiAlignment:                              2/  1000, (   60.00 +/-    42.38) kHz
Hlt1MaterialVertexSeedsDownstreamz:                 0/  1000, (    0.00 +/-     0.00) kHz
Hlt1MaterialVertexSeeds_DWFS:                       0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonJpsiMassAlignment:                        1/  1000, (   30.00 +/-    29.98) kHz
Hlt1OneMuonTrackLine:                               2/  1000, (   60.00 +/-    42.38) kHz
Hlt1BeamGas:                                        0/  1000, (    0.00 +/-     0.00) kHz
Hlt1VeloMicroBias:                                  1/  1000, (   30.00 +/-    29.98) kHz
Hlt1SMOG2BENoBias:                                  0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2PassThroughLowMult5:                       0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2BELowMultElectrons:                        0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2MinimumBias:                               0/  1000, (    0.00 +/-     0.00) kHz
Hlt1PassthroughPVinSMOG2:                           0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2D2Kpi:                                     0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2etacTopp:                                  0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2KsTopipi:                                  0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG22BodyGeneric:                              0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG22BodyGenericPrompt:                        0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2SingleTrackVeryHighPt:                     0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2SingleTrackHighPt:                         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2DiMuonHighMass:                            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2SingleMuon:                                0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2L0Toppi:                                   0/  1000, (    0.00 +/-     0.00) kHz
Inclusive:                                        648/  1000, (19440.00 +/-   453.09) kHz


seed_validator validation:
TrackChecker output                               :      5272/    62216   8.47% ghosts
for P>3GeV,Pt>0.5GeV                              :         0/        0   -nan% ghosts
00_P>3Gev_Pt>0.5                                  :      5289/    35098  15.07% ( 13.36%),        77 (  1.43%) clones, pur  99.17%, hit eff  95.56%
01_long                                           :     22528/    78155  28.82% ( 27.87%),       270 (  1.18%) clones, pur  99.40%, hit eff  96.51%
---1. phi quadrant                                :      5655/    19488  29.02% ( 28.14%),        88 (  1.53%) clones, pur  99.38%, hit eff  96.41%
---2. phi quadrant                                :      5761/    19741  29.18% ( 28.12%),        56 (  0.96%) clones, pur  99.39%, hit eff  96.53%
---3. phi quadrant                                :      5578/    19521  28.57% ( 27.91%),        54 (  0.96%) clones, pur  99.41%, hit eff  96.57%
---4. phi quadrant                                :      5533/    19404  28.51% ( 27.44%),        72 (  1.28%) clones, pur  99.42%, hit eff  96.53%
---eta < 2.5, small x, large y                    :       287/     3192   8.99% (  8.00%),         4 (  1.37%) clones, pur  98.64%, hit eff  93.26%
---eta < 2.5, large x, small y                    :      1144/     5833  19.61% ( 18.61%),        15 (  1.29%) clones, pur  98.92%, hit eff  95.20%
---eta > 2.5, small x, large y                    :      7286/    25071  29.06% ( 28.23%),        89 (  1.21%) clones, pur  99.42%, hit eff  96.60%
---eta > 2.5, large x, small y                    :     13811/    44059  31.35% ( 30.49%),       162 (  1.16%) clones, pur  99.44%, hit eff  96.64%
02_long_P>5GeV                                    :     14511/    51059  28.42% ( 27.36%),       197 (  1.34%) clones, pur  99.38%, hit eff  96.50%
02_long_P>5GeV, eta > 4                           :      8493/    20499  41.43% ( 41.34%),       116 (  1.35%) clones, pur  99.35%, hit eff  96.44%
---eta < 2.5, small x, large y                    :       221/      955  23.14% ( 21.55%),         3 (  1.34%) clones, pur  98.93%, hit eff  95.39%
---eta < 2.5, large x, small y                    :       472/     1992  23.69% ( 22.64%),         5 (  1.05%) clones, pur  99.58%, hit eff  97.42%
---eta > 2.5, small x, large y                    :      4993/    17746  28.14% ( 27.23%),        72 (  1.42%) clones, pur  99.36%, hit eff  96.51%
---eta > 2.5, large x, small y                    :      8825/    30366  29.06% ( 28.12%),       117 (  1.31%) clones, pur  99.39%, hit eff  96.47%
03_long_P>3GeV                                    :     22516/    66923  33.64% ( 32.56%),       270 (  1.18%) clones, pur  99.40%, hit eff  96.51%
04_long_P>0.5GeV                                  :     22528/    78155  28.82% ( 27.87%),       270 (  1.18%) clones, pur  99.40%, hit eff  96.51%
05_long_from_B                                    :       741/     4585  16.16% ( 15.95%),         6 (  0.80%) clones, pur  99.54%, hit eff  96.75%
06_long_from_B_P>5GeV                             :       531/     3752  14.15% ( 14.20%),         4 (  0.75%) clones, pur  99.60%, hit eff  96.90%
07_long_from_B_P>3GeV                             :       741/     4268  17.36% ( 16.98%),         6 (  0.80%) clones, pur  99.54%, hit eff  96.75%
08_UT+SciFi                                       :      6119/    11057  55.34% ( 55.42%),        74 (  1.19%) clones, pur  99.27%, hit eff  95.80%
09_UT+SciFi_P>5GeV                                :      4025/     4628  86.97% ( 88.38%),        56 (  1.37%) clones, pur  99.24%, hit eff  96.04%
10_UT+SciFi_P>3GeV                                :      6095/     7697  79.19% ( 80.17%),        74 (  1.20%) clones, pur  99.27%, hit eff  95.82%
11_UT+SciFi_fromStrange                           :      2747/     3991  68.83% ( 69.75%),        36 (  1.29%) clones, pur  99.40%, hit eff  96.34%
12_UT+SciFi_fromStrange_P>5GeV                    :      2028/     2245  90.33% ( 91.48%),        30 (  1.46%) clones, pur  99.35%, hit eff  96.45%
13_UT+SciFi_fromStrange_P>3GeV                    :      2744/     3200  85.75% ( 86.98%),        36 (  1.29%) clones, pur  99.40%, hit eff  96.35%
14_long_electrons                                 :      2225/     5658  39.32% ( 38.13%),        20 (  0.89%) clones, pur  99.51%, hit eff  96.92%
15_long_electrons_P>5GeV                          :      1561/     2953  52.86% ( 51.38%),        19 (  1.20%) clones, pur  99.50%, hit eff  96.87%
16_long_electrons_P>3GeV                          :      2225/     4554  48.86% ( 48.07%),        20 (  0.89%) clones, pur  99.51%, hit eff  96.92%
17_long_fromB_electrons                           :        78/      240  32.50% ( 30.95%),         1 (  1.27%) clones, pur  99.18%, hit eff  96.67%
18_long_fromB_electrons_P>5GeV                    :        62/      168  36.90% ( 35.60%),         1 (  1.59%) clones, pur  99.54%, hit eff  96.62%
19_long_PT>2GeV                                   :       387/     3268  11.84% ( 11.41%),         6 (  1.53%) clones, pur  99.35%, hit eff  96.42%
20_long_from_B_PT>2GeV                            :       130/     1172  11.09% ( 11.13%),         0 (  0.00%) clones, pur  99.65%, hit eff  97.24%
21_long_strange_P>5GeV                            :       764/     1834  41.66% ( 41.18%),        12 (  1.55%) clones, pur  99.43%, hit eff  96.52%
22_long_strange_P>5GeV_PT>500MeV                  :       120/      738  16.26% ( 15.99%),         4 (  3.23%) clones, pur  99.30%, hit eff  96.06%
23_noVelo+UT+T_fromSignal                         :       182/      322  56.52% ( 54.93%),         2 (  1.09%) clones, pur  98.91%, hit eff  95.94%
24_noVelo+UT+T_fromKs0                            :      1694/     2449  69.17% ( 69.24%),        19 (  1.11%) clones, pur  99.46%, hit eff  96.32%
25_noVelo+UT+T_fromLambda                         :       986/     1415  69.68% ( 70.59%),        16 (  1.60%) clones, pur  99.26%, hit eff  96.20%
26_noVelo+UT+T_fromSignal_P>5GeV                  :       121/      137  88.32% ( 88.11%),         1 (  0.82%) clones, pur  98.91%, hit eff  96.27%
27_noVelo+UT+T_fromKs0_P>5GeV                     :      1229/     1339  91.78% ( 92.70%),        14 (  1.13%) clones, pur  99.44%, hit eff  96.55%
28_noVelo+UT+T_fromLambda_P>5GeV                  :       765/      846  90.43% ( 91.57%),        14 (  1.80%) clones, pur  99.21%, hit eff  96.20%
29_noVelo+UT+T_fromSignal_P>5GeV_PT>500MeV        :        83/       97  85.57% ( 85.31%),         1 (  1.19%) clones, pur  98.42%, hit eff  95.47%
30_noVelo+UT+T_fromKs0_P>5GeV_PT>500MeV           :       665/      730  91.10% ( 91.28%),         8 (  1.19%) clones, pur  99.38%, hit eff  96.51%
31_noVelo+UT+T_fromLambda_P>5GeV_PT>500MeV        :       530/      588  90.14% ( 91.21%),        11 (  2.03%) clones, pur  99.25%, hit eff  96.25%
32_noVelo+UT+T_fromSignal_P>5GeV_PT>500MeV_electrons:         0/        1   0.00% (  0.00%),         0 (  0.00%) clones, pur   -nan%, hit eff   -nan%


selreport_validator validation:
                                               Events  Candidates
Hlt1TrackMVA:                                     251         382
Hlt1TwoTrackMVA:                                  598        2127
Hlt1D2KK:                                          31          32
Hlt1D2KPi:                                         39          40
Hlt1D2PiPi:                                        23          24
Hlt1Dst2D0Pi:                                       2           2
Hlt1KsToPiPi:                                       0           0
Hlt1KsToPiPiDoubleMuonMisID:                        0           0
Hlt1TwoTrackKs:                                     8           9
Hlt1TwoKs:                                          2           2
Hlt1LambdaLLDetachedTrack:                          1           1
Hlt1XiOmegaLLL:                                     0           0
Hlt1SingleHighPtMuon:                               3           3
Hlt1SingleHighPtMuonNoMuID:                         9          11
Hlt1DiMuonHighMass:                                19          31
Hlt1DiMuonDisplaced:                               32          45
Hlt1DiMuonSoft:                                     3           4
Hlt1TrackMuonMVA:                                  17          19
Hlt1DiMuonNoIP:                                     5           6
Hlt1DiMuonNoIP_SS:                                  0           0
Hlt1DiMuonDrellYan_VLowMass:                        0           0
Hlt1DiMuonDrellYan_VLowMass_SS:                     0           0
Hlt1DiMuonDrellYan:                                 0           0
Hlt1DiMuonDrellYan_SS:                              0           0
Hlt1DetJpsiToMuMuPosTagLine:                        1           1
Hlt1DetJpsiToMuMuNegTagLine:                        1           1
Hlt1TrackElectronMVA:                              56          57
Hlt1SingleHighPtElectron:                           8          14
Hlt1DiElectronDisplaced:                           17          29
Hlt1DiPhotonHighMass:                              13          20
Hlt1Pi02GammaGamma:                                 0           0
Hlt1DiElectronHighMass_SS:                          1           3
Hlt1DiElectronHighMass:                             1           4
Hlt1DiElectronLowMass_massSlice1_prompt:            1           1
Hlt1DiElectronLowMass_SS_massSlice1_prompt:         0           0
Hlt1DiElectronLowMass_massSlice2_prompt:            0           0
Hlt1DiElectronLowMass_SS_massSlice2_prompt:         0           0
Hlt1DiElectronLowMass_massSlice3_prompt:            1           1
Hlt1DiElectronLowMass_SS_massSlice3_prompt:         0           0
Hlt1DiElectronLowMass_massSlice4_prompt:            0           0
Hlt1DiElectronLowMass_SS_massSlice4_prompt:         0           0
Hlt1DiElectronLowMass_massSlice1_displaced:         1           1
Hlt1DiElectronLowMass_SS_massSlice1_displaced:      0           0
Hlt1DiElectronLowMass_massSlice2_displaced:         0           0
Hlt1DiElectronLowMass_SS_massSlice2_displaced:      0           0
Hlt1DiElectronLowMass_massSlice3_displaced:         0           0
Hlt1DiElectronLowMass_SS_massSlice3_displaced:      0           0
Hlt1DiElectronLowMass_massSlice4_displaced:         0           0
Hlt1DiElectronLowMass_SS_massSlice4_displaced:      0           0
Hlt1Passthrough:                                    0           0
Hlt1TAEPassthrough:                                 0           0
Hlt1BGIPseudoPVsNoBeam:                             0           0
Hlt1BGIPseudoPVsBeamOne:                            0           0
Hlt1BGIPseudoPVsBeamTwo:                            0           0
Hlt1BGIPseudoPVsUpBeamBeam:                         0           0
Hlt1BGIPseudoPVsDownBeamBeam:                       0           0
Hlt1ODINLumi:                                       0           0
Hlt1ODIN1kHzLumi:                                   0           0
Hlt1ODINCalib:                                      0           0
Hlt1ErrorBank:                                      0           0
Hlt1VeloMicroBiasVeloClosing:                       0           0
Hlt1RICH1Alignment:                                 2           3
Hlt1RICH2Alignment:                                 0           0
Hlt1D2KPiAlignment:                                12          12
Hlt1Dst2D0PiAlignment:                              2           2
Hlt1MaterialVertexSeedsDownstreamz:                 0           0
Hlt1MaterialVertexSeeds_DWFS:                       0           0
Hlt1DiMuonJpsiMassAlignment:                        1           1
Hlt1OneMuonTrackLine:                               2           0
Hlt1BeamGas:                                        0           0
Hlt1VeloMicroBias:                                  1           0
Hlt1SMOG2BENoBias:                                  0           0
Hlt1SMOG2PassThroughLowMult5:                       0           0
Hlt1SMOG2BELowMultElectrons:                        0           0
Hlt1SMOG2MinimumBias:                               0           0
Hlt1PassthroughPVinSMOG2:                           0           0
Hlt1SMOG2D2Kpi:                                     0           0
Hlt1SMOG2etacTopp:                                  0           0
Hlt1SMOG2KsTopipi:                                  0           0
Hlt1SMOG22BodyGeneric:                              0           0
Hlt1SMOG22BodyGenericPrompt:                        0           0
Hlt1SMOG2SingleTrackVeryHighPt:                     0           0
Hlt1SMOG2SingleTrackHighPt:                         0           0
Hlt1SMOG2DiMuonHighMass:                            0           0
Hlt1SMOG2SingleMuon:                                0           0
Hlt1SMOG2L0Toppi:                                   0           0

Total decisions:      1164
Total tracks:         2482
Total calos clusters: 33
Total SVs:            2311
Total hits:           62976
Total stdinfo:        30396


velo_validator validation:
TrackChecker output                               :     20451/   950433   2.15% ghosts
01_velo                                           :    400734/   407433  98.36% ( 98.46%),     11844 (  2.87%) clones, pur  99.65%, hit eff  95.41%
02_long                                           :    232857/   234465  99.31% ( 99.36%),      5397 (  2.27%) clones, pur  99.77%, hit eff  96.60%
03_long_P>5GeV                                    :    152580/   153177  99.61% ( 99.63%),      3297 (  2.12%) clones, pur  99.79%, hit eff  97.12%
04_long_strange                                   :     11253/    11574  97.23% ( 97.77%),       279 (  2.42%) clones, pur  99.36%, hit eff  96.10%
05_long_strange_P>5GeV                            :      5349/     5502  97.22% ( 97.05%),       105 (  1.93%) clones, pur  99.17%, hit eff  96.78%
06_long_fromB                                     :     13650/    13755  99.24% ( 99.40%),       270 (  1.94%) clones, pur  99.70%, hit eff  96.99%
07_long_fromB_P>5GeV                              :     11214/    11256  99.63% ( 99.70%),       216 (  1.89%) clones, pur  99.75%, hit eff  97.26%
08_long_electrons                                 :     16428/    16974  96.78% ( 97.18%),       723 (  4.22%) clones, pur  97.76%, hit eff  95.41%
09_long_fromB_electrons                           :       696/      720  96.67% ( 97.25%),        27 (  3.73%) clones, pur  98.80%, hit eff  96.23%
10_long_fromB_electrons_P>5GeV                    :       492/      504  97.62% ( 98.21%),        15 (  2.96%) clones, pur  98.75%, hit eff  96.84%
11_long_fromSignal                                :      7812/     7863  99.35% ( 99.45%),       153 (  1.92%) clones, pur  99.62%, hit eff  97.11%

