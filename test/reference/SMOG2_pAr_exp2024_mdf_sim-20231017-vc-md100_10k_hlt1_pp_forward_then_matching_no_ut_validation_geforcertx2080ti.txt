long_validator validation:
TrackChecker output                               :       322/     8044   4.00% ghosts
for P>3GeV,Pt>0.5GeV                              :        45/     2675   1.68% ghosts
01_long                                           :      6685/     9180  72.82% ( 72.77%),       162 (  2.37%) clones, pur  99.92%, hit eff  98.76%
02_long_P>5GeV                                    :      5463/     6014  90.84% ( 90.61%),       136 (  2.43%) clones, pur  99.93%, hit eff  99.24%
03_long_strange                                   :       319/      471  67.73% ( 66.92%),         5 (  1.54%) clones, pur  99.82%, hit eff  98.72%
04_long_strange_P>5GeV                            :       214/      240  89.17% ( 88.22%),         4 (  1.83%) clones, pur  99.79%, hit eff  99.25%
07_long_electrons                                 :       432/     1194  36.18% ( 37.63%),        26 (  5.68%) clones, pur  98.63%, hit eff  99.04%
08_long_electrons_P>5GeV                          :       370/      700  52.86% ( 54.33%),        26 (  6.57%) clones, pur  98.62%, hit eff  99.37%
long_P>5GeV_AND_Pt>1GeV                           :       494/      511  96.67% ( 96.67%),         9 (  1.79%) clones, pur  99.93%, hit eff  98.99%
11_noVelo_UT                                      :         0/     1313   0.00% (  0.00%),         0 (  0.00%) clones, pur   -nan%, hit eff   -nan%
12_noVelo_UT_P>5GeV                               :         0/      549   0.00% (  0.00%),         0 (  0.00%) clones, pur   -nan%, hit eff   -nan%
13_long_PT>2GeV                                   :        27/       28  96.43% ( 98.67%),         1 (  3.57%) clones, pur 100.00%, hit eff  99.70%
15_long_strange_P>5GeV                            :       214/      240  89.17% ( 88.22%),         4 (  1.83%) clones, pur  99.79%, hit eff  99.25%
16_long_strange_P>5GeV_PT>500MeV                  :        64/       69  92.75% ( 93.33%),         1 (  1.54%) clones, pur  99.90%, hit eff  99.00%


muon_validator validation:
Muon fraction in all MCPs:                                               1690/   147472   0.01% 
Muon fraction in MCPs to which a track(s) was matched:                     77/     8756   0.01% 
Correctly identified muons with isMuon:                                    74/       77  96.10% 
Correctly identified muons from strange decays with isMuon:                 1/        1 100.00% 
Correctly identified muons from B decays with isMuon:                       0/        0   -nan% 
Tracks identified as muon with isMuon, but matched to non-muon MCP:       286/     8679   3.30% 
Ghost tracks identified as muon with isMuon:                                9/      322   2.80% 


pv_validator validation:
REC and MC vertices matched by dz distance
MC PV is reconstructible if at least 4 tracks are reconstructed
MC PV is isolated if dz to closest reconstructible MC PV > 10.00 mm
REC and MC vertices matched by dz distance

All                  :  0.923 (   896/   971)
Isolated             :  0.940 (   866/   921)
Close                :  0.600 (    30/    50)
False rate           :  0.012 (    11/   907)
Real false rate      :  0.012 (    11/   907)
Clones               :  0.000 (     0/   896)


rate_validator validation:
Hlt1TrackMVA:                                       0/  1000, (    0.00 +/-     0.00) kHz
Hlt1TwoTrackMVA:                                    0/  1000, (    0.00 +/-     0.00) kHz
Hlt1D2KK:                                           0/  1000, (    0.00 +/-     0.00) kHz
Hlt1D2KPi:                                          0/  1000, (    0.00 +/-     0.00) kHz
Hlt1D2PiPi:                                         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1Dst2D0Pi:                                       0/  1000, (    0.00 +/-     0.00) kHz
Hlt1KsToPiPi:                                       0/  1000, (    0.00 +/-     0.00) kHz
Hlt1KsToPiPiDoubleMuonMisID:                        0/  1000, (    0.00 +/-     0.00) kHz
Hlt1TwoTrackKs:                                     0/  1000, (    0.00 +/-     0.00) kHz
Hlt1TwoKs:                                          0/  1000, (    0.00 +/-     0.00) kHz
Hlt1LambdaLLDetachedTrack:                          0/  1000, (    0.00 +/-     0.00) kHz
Hlt1XiOmegaLLL:                                     0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SingleHighPtMuon:                               0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SingleHighPtMuonNoMuID:                         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonHighMass:                                 0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDisplaced:                                0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonSoft:                                     0/  1000, (    0.00 +/-     0.00) kHz
Hlt1TrackMuonMVA:                                   0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonNoIP:                                     0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonNoIP_SS:                                  0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDrellYan_VLowMass:                        0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDrellYan_VLowMass_SS:                     0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDrellYan:                                 0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDrellYan_SS:                              0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DetJpsiToMuMuPosTagLine:                        0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DetJpsiToMuMuNegTagLine:                        0/  1000, (    0.00 +/-     0.00) kHz
Hlt1TrackElectronMVA:                               0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SingleHighPtElectron:                           0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronDisplaced:                            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiPhotonHighMass:                               0/  1000, (    0.00 +/-     0.00) kHz
Hlt1Pi02GammaGamma:                                 0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronHighMass_SS:                          0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronHighMass:                             0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice1_prompt:            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice1_prompt:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice2_prompt:            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice2_prompt:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice3_prompt:            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice3_prompt:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice4_prompt:            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice4_prompt:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice1_displaced:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice1_displaced:      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice2_displaced:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice2_displaced:      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice3_displaced:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice3_displaced:      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice4_displaced:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice4_displaced:      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1Passthrough:                                    0/  1000, (    0.00 +/-     0.00) kHz
Hlt1TAEPassthrough:                                 0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsNoBeam:                             0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsBeamOne:                           11/  1000, (  330.00 +/-    98.95) kHz
Hlt1BGIPseudoPVsBeamTwo:                            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsUpBeamBeam:                         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsDownBeamBeam:                       0/  1000, (    0.00 +/-     0.00) kHz
Hlt1ODINLumi:                                       0/  1000, (    0.00 +/-     0.00) kHz
Hlt1ODIN1kHzLumi:                                   0/  1000, (    0.00 +/-     0.00) kHz
Hlt1ODINCalib:                                      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1ErrorBank:                                      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1VeloMicroBiasVeloClosing:                       0/  1000, (    0.00 +/-     0.00) kHz
Hlt1RICH1Alignment:                                 0/  1000, (    0.00 +/-     0.00) kHz
Hlt1RICH2Alignment:                                 0/  1000, (    0.00 +/-     0.00) kHz
Hlt1D2KPiAlignment:                                 0/  1000, (    0.00 +/-     0.00) kHz
Hlt1Dst2D0PiAlignment:                              0/  1000, (    0.00 +/-     0.00) kHz
Hlt1MaterialVertexSeedsDownstreamz:                 0/  1000, (    0.00 +/-     0.00) kHz
Hlt1MaterialVertexSeeds_DWFS:                       0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonJpsiMassAlignment:                        0/  1000, (    0.00 +/-     0.00) kHz
Hlt1OneMuonTrackLine:                               0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BeamGas:                                        0/  1000, (    0.00 +/-     0.00) kHz
Hlt1VeloMicroBias:                                  1/  1000, (   30.00 +/-    29.98) kHz
Hlt1SMOG2BENoBias:                                  0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2PassThroughLowMult5:                      11/  1000, (  330.00 +/-    98.95) kHz
Hlt1SMOG2BELowMultElectrons:                        3/  1000, (   90.00 +/-    51.88) kHz
Hlt1SMOG2MinimumBias:                               0/  1000, (    0.00 +/-     0.00) kHz
Hlt1PassthroughPVinSMOG2:                           0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2D2Kpi:                                     9/  1000, (  270.00 +/-    89.59) kHz
Hlt1SMOG2etacTopp:                                  1/  1000, (   30.00 +/-    29.98) kHz
Hlt1SMOG2KsTopipi:                                  3/  1000, (   90.00 +/-    51.88) kHz
Hlt1SMOG22BodyGeneric:                              5/  1000, (  150.00 +/-    66.91) kHz
Hlt1SMOG22BodyGenericPrompt:                        3/  1000, (   90.00 +/-    51.88) kHz
Hlt1SMOG2SingleTrackVeryHighPt:                     0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2SingleTrackHighPt:                         1/  1000, (   30.00 +/-    29.98) kHz
Hlt1SMOG2DiMuonHighMass:                            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2SingleMuon:                                2/  1000, (   60.00 +/-    42.38) kHz
Hlt1SMOG2L0Toppi:                                   4/  1000, (  120.00 +/-    59.88) kHz
Inclusive:                                         52/  1000, ( 1560.00 +/-   210.63) kHz


seed_validator validation:
TrackChecker output                               :        42/    13587   0.31% ghosts
for P>3GeV,Pt>0.5GeV                              :         0/        0   -nan% ghosts
00_P>3Gev_Pt>0.5                                  :      2361/     2793  84.53% ( 85.71%),        10 (  0.42%) clones, pur  99.77%, hit eff  98.61%
01_long                                           :      7116/     9180  77.52% ( 78.19%),        21 (  0.29%) clones, pur  99.82%, hit eff  98.63%
---1. phi quadrant                                :      1817/     2319  78.35% ( 79.15%),         7 (  0.38%) clones, pur  99.78%, hit eff  98.50%
---2. phi quadrant                                :      1830/     2356  77.67% ( 77.60%),         2 (  0.11%) clones, pur  99.84%, hit eff  98.60%
---3. phi quadrant                                :      1752/     2259  77.56% ( 78.68%),         6 (  0.34%) clones, pur  99.83%, hit eff  98.74%
---4. phi quadrant                                :      1717/     2245  76.48% ( 76.20%),         6 (  0.35%) clones, pur  99.85%, hit eff  98.70%
---eta < 2.5, small x, large y                    :         9/      130   6.92% (  7.60%),         0 (  0.00%) clones, pur  97.88%, hit eff  88.89%
---eta < 2.5, large x, small y                    :        46/      176  26.14% ( 27.42%),         0 (  0.00%) clones, pur  99.82%, hit eff  95.45%
---eta > 2.5, small x, large y                    :      2568/     3254  78.92% ( 79.07%),         9 (  0.35%) clones, pur  99.82%, hit eff  98.34%
---eta > 2.5, large x, small y                    :      4493/     5620  79.95% ( 80.59%),        12 (  0.27%) clones, pur  99.83%, hit eff  98.85%
02_long_P>5GeV                                    :      5603/     6014  93.17% ( 93.78%),        16 (  0.28%) clones, pur  99.83%, hit eff  99.20%
02_long_P>5GeV, eta > 4                           :      3045/     3227  94.36% ( 93.93%),        10 (  0.33%) clones, pur  99.81%, hit eff  99.38%
---eta < 2.5, small x, large y                    :         7/       14  50.00% ( 50.00%),         0 (  0.00%) clones, pur  98.70%, hit eff  89.29%
---eta < 2.5, large x, small y                    :        15/       18  83.33% ( 83.33%),         0 (  0.00%) clones, pur 100.00%, hit eff  98.89%
---eta > 2.5, small x, large y                    :      2077/     2207  94.11% ( 94.79%),         8 (  0.38%) clones, pur  99.81%, hit eff  99.12%
---eta > 2.5, large x, small y                    :      3504/     3775  92.82% ( 93.08%),         8 (  0.23%) clones, pur  99.84%, hit eff  99.27%
03_long_P>3GeV                                    :      7116/     7928  89.76% ( 90.19%),        21 (  0.29%) clones, pur  99.82%, hit eff  98.63%
04_long_P>0.5GeV                                  :      7116/     9180  77.52% ( 78.19%),        21 (  0.29%) clones, pur  99.82%, hit eff  98.63%
08_UT+SciFi                                       :       951/     1925  49.40% ( 45.59%),         2 (  0.21%) clones, pur  99.76%, hit eff  96.99%
09_UT+SciFi_P>5GeV                                :       587/      656  89.48% ( 89.26%),         1 (  0.17%) clones, pur  99.75%, hit eff  97.68%
10_UT+SciFi_P>3GeV                                :       946/     1248  75.80% ( 73.33%),         2 (  0.21%) clones, pur  99.76%, hit eff  96.99%
11_UT+SciFi_fromStrange                           :       204/      289  70.59% ( 71.50%),         0 (  0.00%) clones, pur  99.73%, hit eff  98.27%
12_UT+SciFi_fromStrange_P>5GeV                    :       154/      160  96.25% ( 96.89%),         0 (  0.00%) clones, pur  99.76%, hit eff  98.69%
13_UT+SciFi_fromStrange_P>3GeV                    :       204/      238  85.71% ( 84.39%),         0 (  0.00%) clones, pur  99.73%, hit eff  98.27%
14_long_electrons                                 :       631/     1194  52.85% ( 52.84%),         3 (  0.47%) clones, pur  99.76%, hit eff  98.94%
15_long_electrons_P>5GeV                          :       511/      700  73.00% ( 72.48%),         3 (  0.58%) clones, pur  99.76%, hit eff  99.10%
16_long_electrons_P>3GeV                          :       631/     1022  61.74% ( 62.17%),         3 (  0.47%) clones, pur  99.76%, hit eff  98.94%
19_long_PT>2GeV                                   :        23/       28  82.14% ( 82.67%),         0 (  0.00%) clones, pur 100.00%, hit eff  99.64%
21_long_strange_P>5GeV                            :       227/      240  94.58% ( 94.14%),         1 (  0.44%) clones, pur  99.74%, hit eff  99.23%
22_long_strange_P>5GeV_PT>500MeV                  :        62/       69  89.86% ( 90.83%),         0 (  0.00%) clones, pur  99.84%, hit eff  99.34%
24_noVelo+UT+T_fromKs0                            :       147/      204  72.06% ( 70.49%),         0 (  0.00%) clones, pur  99.82%, hit eff  98.30%
25_noVelo+UT+T_fromLambda                         :        66/       99  66.67% ( 65.93%),         0 (  0.00%) clones, pur  99.59%, hit eff  98.32%
27_noVelo+UT+T_fromKs0_P>5GeV                     :       107/      109  98.17% ( 98.53%),         0 (  0.00%) clones, pur  99.75%, hit eff  98.73%
28_noVelo+UT+T_fromLambda_P>5GeV                  :        51/       53  96.23% ( 96.59%),         0 (  0.00%) clones, pur  99.82%, hit eff  98.84%
30_noVelo+UT+T_fromKs0_P>5GeV_PT>500MeV           :        52/       53  98.11% ( 97.87%),         0 (  0.00%) clones, pur  99.65%, hit eff  98.53%
31_noVelo+UT+T_fromLambda_P>5GeV_PT>500MeV        :        27/       29  93.10% ( 92.31%),         0 (  0.00%) clones, pur  99.66%, hit eff  98.43%


selreport_validator validation:
                                               Events  Candidates
Hlt1TrackMVA:                                       0           0
Hlt1TwoTrackMVA:                                    0           0
Hlt1D2KK:                                           0           0
Hlt1D2KPi:                                          0           0
Hlt1D2PiPi:                                         0           0
Hlt1Dst2D0Pi:                                       0           0
Hlt1KsToPiPi:                                       0           0
Hlt1KsToPiPiDoubleMuonMisID:                        0           0
Hlt1TwoTrackKs:                                     0           0
Hlt1TwoKs:                                          0           0
Hlt1LambdaLLDetachedTrack:                          0           0
Hlt1XiOmegaLLL:                                     0           0
Hlt1SingleHighPtMuon:                               0           0
Hlt1SingleHighPtMuonNoMuID:                         0           0
Hlt1DiMuonHighMass:                                 0           0
Hlt1DiMuonDisplaced:                                0           0
Hlt1DiMuonSoft:                                     0           0
Hlt1TrackMuonMVA:                                   0           0
Hlt1DiMuonNoIP:                                     0           0
Hlt1DiMuonNoIP_SS:                                  0           0
Hlt1DiMuonDrellYan_VLowMass:                        0           0
Hlt1DiMuonDrellYan_VLowMass_SS:                     0           0
Hlt1DiMuonDrellYan:                                 0           0
Hlt1DiMuonDrellYan_SS:                              0           0
Hlt1DetJpsiToMuMuPosTagLine:                        0           0
Hlt1DetJpsiToMuMuNegTagLine:                        0           0
Hlt1TrackElectronMVA:                               0           0
Hlt1SingleHighPtElectron:                           0           0
Hlt1DiElectronDisplaced:                            0           0
Hlt1DiPhotonHighMass:                               0           0
Hlt1Pi02GammaGamma:                                 0           0
Hlt1DiElectronHighMass_SS:                          0           0
Hlt1DiElectronHighMass:                             0           0
Hlt1DiElectronLowMass_massSlice1_prompt:            0           0
Hlt1DiElectronLowMass_SS_massSlice1_prompt:         0           0
Hlt1DiElectronLowMass_massSlice2_prompt:            0           0
Hlt1DiElectronLowMass_SS_massSlice2_prompt:         0           0
Hlt1DiElectronLowMass_massSlice3_prompt:            0           0
Hlt1DiElectronLowMass_SS_massSlice3_prompt:         0           0
Hlt1DiElectronLowMass_massSlice4_prompt:            0           0
Hlt1DiElectronLowMass_SS_massSlice4_prompt:         0           0
Hlt1DiElectronLowMass_massSlice1_displaced:         0           0
Hlt1DiElectronLowMass_SS_massSlice1_displaced:      0           0
Hlt1DiElectronLowMass_massSlice2_displaced:         0           0
Hlt1DiElectronLowMass_SS_massSlice2_displaced:      0           0
Hlt1DiElectronLowMass_massSlice3_displaced:         0           0
Hlt1DiElectronLowMass_SS_massSlice3_displaced:      0           0
Hlt1DiElectronLowMass_massSlice4_displaced:         0           0
Hlt1DiElectronLowMass_SS_massSlice4_displaced:      0           0
Hlt1Passthrough:                                    0           0
Hlt1TAEPassthrough:                                 0           0
Hlt1BGIPseudoPVsNoBeam:                             0           0
Hlt1BGIPseudoPVsBeamOne:                           11           0
Hlt1BGIPseudoPVsBeamTwo:                            0           0
Hlt1BGIPseudoPVsUpBeamBeam:                         0           0
Hlt1BGIPseudoPVsDownBeamBeam:                       0           0
Hlt1ODINLumi:                                       0           0
Hlt1ODIN1kHzLumi:                                   0           0
Hlt1ODINCalib:                                      0           0
Hlt1ErrorBank:                                      0           0
Hlt1VeloMicroBiasVeloClosing:                       0           0
Hlt1RICH1Alignment:                                 0           0
Hlt1RICH2Alignment:                                 0           0
Hlt1D2KPiAlignment:                                 0           0
Hlt1Dst2D0PiAlignment:                              0           0
Hlt1MaterialVertexSeedsDownstreamz:                 0           0
Hlt1MaterialVertexSeeds_DWFS:                       0           0
Hlt1DiMuonJpsiMassAlignment:                        0           0
Hlt1OneMuonTrackLine:                               0           0
Hlt1BeamGas:                                        0           0
Hlt1VeloMicroBias:                                  1           0
Hlt1SMOG2BENoBias:                                  0           0
Hlt1SMOG2PassThroughLowMult5:                      11           0
Hlt1SMOG2BELowMultElectrons:                        3           0
Hlt1SMOG2MinimumBias:                               0           0
Hlt1PassthroughPVinSMOG2:                           0           0
Hlt1SMOG2D2Kpi:                                     9          12
Hlt1SMOG2etacTopp:                                  1           1
Hlt1SMOG2KsTopipi:                                  3           3
Hlt1SMOG22BodyGeneric:                              5          20
Hlt1SMOG22BodyGenericPrompt:                        3           6
Hlt1SMOG2SingleTrackVeryHighPt:                     0           0
Hlt1SMOG2SingleTrackHighPt:                         1           1
Hlt1SMOG2DiMuonHighMass:                            0           0
Hlt1SMOG2SingleMuon:                                2           2
Hlt1SMOG2L0Toppi:                                   4           4

Total decisions:      54
Total tracks:         67
Total calos clusters: 0
Total SVs:            45
Total hits:           1833
Total stdinfo:        770


velo_validator validation:
TrackChecker output                               :       488/    55326   0.88% ghosts
01_velo                                           :     30142/    30766  97.97% ( 98.26%),      1358 (  4.31%) clones, pur  99.55%, hit eff  94.46%
02_long                                           :     18202/    18360  99.14% ( 99.34%),       688 (  3.64%) clones, pur  99.74%, hit eff  95.31%
03_long_P>5GeV                                    :     11972/    12028  99.53% ( 99.59%),       396 (  3.20%) clones, pur  99.81%, hit eff  96.03%
04_long_strange                                   :       922/      942  97.88% ( 97.93%),        30 (  3.15%) clones, pur  99.50%, hit eff  95.36%
05_long_strange_P>5GeV                            :       466/      480  97.08% ( 96.55%),         8 (  1.69%) clones, pur  99.38%, hit eff  97.07%
08_long_electrons                                 :      2356/     2388  98.66% ( 98.51%),       200 (  7.82%) clones, pur  97.33%, hit eff  93.61%

