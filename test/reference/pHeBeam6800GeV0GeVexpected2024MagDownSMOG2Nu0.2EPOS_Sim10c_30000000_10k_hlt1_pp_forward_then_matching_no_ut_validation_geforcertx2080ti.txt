long_validator validation:
TrackChecker output                               :       119/     4095   2.91% ghosts
for P>3GeV,Pt>0.5GeV                              :         7/     1282   0.55% ghosts
01_long                                           :      3424/     4633  73.90% ( 74.11%),        95 (  2.70%) clones, pur  99.94%, hit eff  98.97%
02_long_P>5GeV                                    :      2803/     3074  91.18% ( 91.58%),        85 (  2.94%) clones, pur  99.96%, hit eff  99.33%
03_long_strange                                   :       119/      192  61.98% ( 61.20%),         5 (  4.03%) clones, pur  99.92%, hit eff  99.63%
04_long_strange_P>5GeV                            :        94/      107  87.85% ( 86.14%),         5 (  5.05%) clones, pur  99.97%, hit eff  99.57%
07_long_electrons                                 :       207/      605  34.21% ( 33.80%),         8 (  3.72%) clones, pur  99.26%, hit eff  99.00%
08_long_electrons_P>5GeV                          :       175/      332  52.71% ( 53.03%),         8 (  4.37%) clones, pur  99.20%, hit eff  99.19%
long_P>5GeV_AND_Pt>1GeV                           :       207/      213  97.18% ( 98.37%),         5 (  2.36%) clones, pur  99.93%, hit eff  99.62%
11_noVelo_UT                                      :         0/      647   0.00% (  0.00%),         0 (  0.00%) clones, pur   -nan%, hit eff   -nan%
12_noVelo_UT_P>5GeV                               :         0/      255   0.00% (  0.00%),         0 (  0.00%) clones, pur   -nan%, hit eff   -nan%
13_long_PT>2GeV                                   :        12/       12 100.00% (100.00%),         0 (  0.00%) clones, pur 100.00%, hit eff  99.31%
15_long_strange_P>5GeV                            :        94/      107  87.85% ( 86.14%),         5 (  5.05%) clones, pur  99.97%, hit eff  99.57%
16_long_strange_P>5GeV_PT>500MeV                  :        22/       24  91.67% ( 91.67%),         1 (  4.35%) clones, pur  99.87%, hit eff  98.08%


muon_validator validation:
Muon fraction in all MCPs:                                                919/    76498   0.01% 
Muon fraction in MCPs to which a track(s) was matched:                     44/     4473   0.01% 
Correctly identified muons with isMuon:                                    41/       44  93.18% 
Correctly identified muons from strange decays with isMuon:                 0/        0   -nan% 
Correctly identified muons from B decays with isMuon:                       0/        0   -nan% 
Tracks identified as muon with isMuon, but matched to non-muon MCP:       112/     4429   2.53% 
Ghost tracks identified as muon with isMuon:                                3/      119   2.52% 


pv_validator validation:
REC and MC vertices matched by dz distance
MC PV is reconstructible if at least 4 tracks are reconstructed
MC PV is isolated if dz to closest reconstructible MC PV > 10.00 mm
REC and MC vertices matched by dz distance

All                  :  0.949 (   737/   777)
Isolated             :  0.959 (   724/   755)
Close                :  0.591 (    13/    22)
False rate           :  0.020 (    15/   752)
Real false rate      :  0.020 (    15/   752)
Clones               :  0.000 (     0/   737)


rate_validator validation:
Hlt1TrackMVA:                                       0/  1000, (    0.00 +/-     0.00) kHz
Hlt1TwoTrackMVA:                                    0/  1000, (    0.00 +/-     0.00) kHz
Hlt1D2KK:                                           0/  1000, (    0.00 +/-     0.00) kHz
Hlt1D2KPi:                                          0/  1000, (    0.00 +/-     0.00) kHz
Hlt1D2PiPi:                                         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1Dst2D0Pi:                                       0/  1000, (    0.00 +/-     0.00) kHz
Hlt1KsToPiPi:                                       0/  1000, (    0.00 +/-     0.00) kHz
Hlt1KsToPiPiDoubleMuonMisID:                        0/  1000, (    0.00 +/-     0.00) kHz
Hlt1TwoTrackKs:                                     0/  1000, (    0.00 +/-     0.00) kHz
Hlt1TwoKs:                                          0/  1000, (    0.00 +/-     0.00) kHz
Hlt1LambdaLLDetachedTrack:                          0/  1000, (    0.00 +/-     0.00) kHz
Hlt1XiOmegaLLL:                                     0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SingleHighPtMuon:                               0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SingleHighPtMuonNoMuID:                         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonHighMass:                                 0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDisplaced:                                0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonSoft:                                     0/  1000, (    0.00 +/-     0.00) kHz
Hlt1TrackMuonMVA:                                   0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonNoIP:                                     0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonNoIP_SS:                                  0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDrellYan_VLowMass:                        0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDrellYan_VLowMass_SS:                     0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDrellYan:                                 0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDrellYan_SS:                              0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DetJpsiToMuMuPosTagLine:                        0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DetJpsiToMuMuNegTagLine:                        0/  1000, (    0.00 +/-     0.00) kHz
Hlt1TrackElectronMVA:                               0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SingleHighPtElectron:                           0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronDisplaced:                            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiPhotonHighMass:                               0/  1000, (    0.00 +/-     0.00) kHz
Hlt1Pi02GammaGamma:                                 0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronHighMass_SS:                          0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronHighMass:                             0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice1_prompt:            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice1_prompt:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice2_prompt:            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice2_prompt:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice3_prompt:            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice3_prompt:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice4_prompt:            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice4_prompt:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice1_displaced:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice1_displaced:      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice2_displaced:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice2_displaced:      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice3_displaced:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice3_displaced:      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice4_displaced:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice4_displaced:      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1Passthrough:                                    0/  1000, (    0.00 +/-     0.00) kHz
Hlt1TAEPassthrough:                                 0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsNoBeam:                             0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsBeamOne:                            6/  1000, (  180.00 +/-    73.26) kHz
Hlt1BGIPseudoPVsBeamTwo:                            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsUpBeamBeam:                         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsDownBeamBeam:                       0/  1000, (    0.00 +/-     0.00) kHz
Hlt1ODINLumi:                                       0/  1000, (    0.00 +/-     0.00) kHz
Hlt1ODIN1kHzLumi:                                   0/  1000, (    0.00 +/-     0.00) kHz
Hlt1ODINCalib:                                      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1ErrorBank:                                      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1VeloMicroBiasVeloClosing:                       0/  1000, (    0.00 +/-     0.00) kHz
Hlt1RICH1Alignment:                                 0/  1000, (    0.00 +/-     0.00) kHz
Hlt1RICH2Alignment:                                 0/  1000, (    0.00 +/-     0.00) kHz
Hlt1D2KPiAlignment:                                 0/  1000, (    0.00 +/-     0.00) kHz
Hlt1Dst2D0PiAlignment:                              0/  1000, (    0.00 +/-     0.00) kHz
Hlt1MaterialVertexSeedsDownstreamz:                 0/  1000, (    0.00 +/-     0.00) kHz
Hlt1MaterialVertexSeeds_DWFS:                       0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonJpsiMassAlignment:                        0/  1000, (    0.00 +/-     0.00) kHz
Hlt1OneMuonTrackLine:                               0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BeamGas:                                        0/  1000, (    0.00 +/-     0.00) kHz
Hlt1VeloMicroBias:                                  0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2BENoBias:                                  0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2PassThroughLowMult5:                      22/  1000, (  660.00 +/-   139.16) kHz
Hlt1SMOG2BELowMultElectrons:                        3/  1000, (   90.00 +/-    51.88) kHz
Hlt1SMOG2MinimumBias:                               0/  1000, (    0.00 +/-     0.00) kHz
Hlt1PassthroughPVinSMOG2:                           0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2D2Kpi:                                     2/  1000, (   60.00 +/-    42.38) kHz
Hlt1SMOG2etacTopp:                                  0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2KsTopipi:                                  1/  1000, (   30.00 +/-    29.98) kHz
Hlt1SMOG22BodyGeneric:                              1/  1000, (   30.00 +/-    29.98) kHz
Hlt1SMOG22BodyGenericPrompt:                        1/  1000, (   30.00 +/-    29.98) kHz
Hlt1SMOG2SingleTrackVeryHighPt:                     0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2SingleTrackHighPt:                         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2DiMuonHighMass:                            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2SingleMuon:                                3/  1000, (   90.00 +/-    51.88) kHz
Hlt1SMOG2L0Toppi:                                   2/  1000, (   60.00 +/-    42.38) kHz
Inclusive:                                         39/  1000, ( 1170.00 +/-   183.66) kHz


seed_validator validation:
TrackChecker output                               :        13/     6971   0.19% ghosts
for P>3GeV,Pt>0.5GeV                              :         0/        0   -nan% ghosts
00_P>3Gev_Pt>0.5                                  :      1160/     1350  85.93% ( 85.69%),         1 (  0.09%) clones, pur  99.87%, hit eff  98.86%
01_long                                           :      3683/     4633  79.49% ( 79.55%),         6 (  0.16%) clones, pur  99.88%, hit eff  98.85%
---1. phi quadrant                                :       962/     1194  80.57% ( 82.79%),         3 (  0.31%) clones, pur  99.83%, hit eff  98.70%
---2. phi quadrant                                :       888/     1117  79.50% ( 78.49%),         0 (  0.00%) clones, pur  99.92%, hit eff  98.72%
---3. phi quadrant                                :       916/     1183  77.43% ( 78.49%),         1 (  0.11%) clones, pur  99.91%, hit eff  98.97%
---4. phi quadrant                                :       917/     1139  80.51% ( 79.15%),         2 (  0.22%) clones, pur  99.87%, hit eff  98.99%
---eta < 2.5, small x, large y                    :         5/       57   8.77% ( 10.42%),         0 (  0.00%) clones, pur 100.00%, hit eff  90.00%
---eta < 2.5, large x, small y                    :        29/       84  34.52% ( 33.79%),         0 (  0.00%) clones, pur 100.00%, hit eff  97.65%
---eta > 2.5, small x, large y                    :      1343/     1689  79.51% ( 79.10%),         4 (  0.30%) clones, pur  99.87%, hit eff  98.56%
---eta > 2.5, large x, small y                    :      2306/     2803  82.27% ( 82.63%),         2 (  0.09%) clones, pur  99.89%, hit eff  99.05%
02_long_P>5GeV                                    :      2891/     3074  94.05% ( 94.42%),         6 (  0.21%) clones, pur  99.90%, hit eff  99.28%
02_long_P>5GeV, eta > 4                           :      1611/     1712  94.10% ( 94.12%),         3 (  0.19%) clones, pur  99.92%, hit eff  99.44%
---eta < 2.5, small x, large y                    :         4/        7  57.14% ( 57.14%),         0 (  0.00%) clones, pur 100.00%, hit eff  87.50%
---eta < 2.5, large x, small y                    :         9/       10  90.00% ( 90.00%),         0 (  0.00%) clones, pur 100.00%, hit eff 100.00%
---eta > 2.5, small x, large y                    :      1102/     1170  94.19% ( 94.13%),         4 (  0.36%) clones, pur  99.88%, hit eff  99.14%
---eta > 2.5, large x, small y                    :      1776/     1887  94.12% ( 94.34%),         2 (  0.11%) clones, pur  99.91%, hit eff  99.39%
03_long_P>3GeV                                    :      3683/     4039  91.19% ( 91.08%),         6 (  0.16%) clones, pur  99.88%, hit eff  98.85%
04_long_P>0.5GeV                                  :      3683/     4633  79.49% ( 79.55%),         6 (  0.16%) clones, pur  99.88%, hit eff  98.85%
08_UT+SciFi                                       :       440/      895  49.16% ( 46.59%),         1 (  0.23%) clones, pur  99.85%, hit eff  97.10%
09_UT+SciFi_P>5GeV                                :       265/      288  92.01% ( 92.03%),         0 (  0.00%) clones, pur  99.90%, hit eff  97.90%
10_UT+SciFi_P>3GeV                                :       439/      565  77.70% ( 76.50%),         1 (  0.23%) clones, pur  99.85%, hit eff  97.09%
11_UT+SciFi_fromStrange                           :        90/      113  79.65% ( 78.22%),         1 (  1.10%) clones, pur  99.80%, hit eff  97.46%
12_UT+SciFi_fromStrange_P>5GeV                    :        62/       64  96.88% ( 98.04%),         0 (  0.00%) clones, pur  99.87%, hit eff  98.28%
13_UT+SciFi_fromStrange_P>3GeV                    :        90/       97  92.78% ( 92.16%),         1 (  1.10%) clones, pur  99.80%, hit eff  97.46%
14_long_electrons                                 :       305/      605  50.41% ( 48.06%),         3 (  0.97%) clones, pur  99.84%, hit eff  98.83%
15_long_electrons_P>5GeV                          :       247/      332  74.40% ( 73.10%),         3 (  1.20%) clones, pur  99.80%, hit eff  98.93%
16_long_electrons_P>3GeV                          :       305/      488  62.50% ( 60.20%),         3 (  0.97%) clones, pur  99.84%, hit eff  98.83%
19_long_PT>2GeV                                   :        10/       12  83.33% ( 83.33%),         0 (  0.00%) clones, pur 100.00%, hit eff  99.17%
21_long_strange_P>5GeV                            :       100/      107  93.46% ( 93.37%),         0 (  0.00%) clones, pur  99.91%, hit eff  99.58%
22_long_strange_P>5GeV_PT>500MeV                  :        20/       24  83.33% ( 83.33%),         0 (  0.00%) clones, pur 100.00%, hit eff  98.33%
24_noVelo+UT+T_fromKs0                            :        61/       83  73.49% ( 73.73%),         0 (  0.00%) clones, pur  99.86%, hit eff  97.16%
25_noVelo+UT+T_fromLambda                         :        20/       26  76.92% ( 78.57%),         1 (  4.76%) clones, pur  99.52%, hit eff  97.19%
27_noVelo+UT+T_fromKs0_P>5GeV                     :        39/       41  95.12% ( 97.14%),         0 (  0.00%) clones, pur  99.79%, hit eff  97.48%
28_noVelo+UT+T_fromLambda_P>5GeV                  :        12/       12 100.00% (100.00%),         0 (  0.00%) clones, pur 100.00%, hit eff 100.00%
30_noVelo+UT+T_fromKs0_P>5GeV_PT>500MeV           :        15/       15 100.00% (100.00%),         0 (  0.00%) clones, pur 100.00%, hit eff  99.44%
31_noVelo+UT+T_fromLambda_P>5GeV_PT>500MeV        :         5/        5 100.00% (100.00%),         0 (  0.00%) clones, pur 100.00%, hit eff 100.00%


selreport_validator validation:
                                               Events  Candidates
Hlt1TrackMVA:                                       0           0
Hlt1TwoTrackMVA:                                    0           0
Hlt1D2KK:                                           0           0
Hlt1D2KPi:                                          0           0
Hlt1D2PiPi:                                         0           0
Hlt1Dst2D0Pi:                                       0           0
Hlt1KsToPiPi:                                       0           0
Hlt1KsToPiPiDoubleMuonMisID:                        0           0
Hlt1TwoTrackKs:                                     0           0
Hlt1TwoKs:                                          0           0
Hlt1LambdaLLDetachedTrack:                          0           0
Hlt1XiOmegaLLL:                                     0           0
Hlt1SingleHighPtMuon:                               0           0
Hlt1SingleHighPtMuonNoMuID:                         0           0
Hlt1DiMuonHighMass:                                 0           0
Hlt1DiMuonDisplaced:                                0           0
Hlt1DiMuonSoft:                                     0           0
Hlt1TrackMuonMVA:                                   0           0
Hlt1DiMuonNoIP:                                     0           0
Hlt1DiMuonNoIP_SS:                                  0           0
Hlt1DiMuonDrellYan_VLowMass:                        0           0
Hlt1DiMuonDrellYan_VLowMass_SS:                     0           0
Hlt1DiMuonDrellYan:                                 0           0
Hlt1DiMuonDrellYan_SS:                              0           0
Hlt1DetJpsiToMuMuPosTagLine:                        0           0
Hlt1DetJpsiToMuMuNegTagLine:                        0           0
Hlt1TrackElectronMVA:                               0           0
Hlt1SingleHighPtElectron:                           0           0
Hlt1DiElectronDisplaced:                            0           0
Hlt1DiPhotonHighMass:                               0           0
Hlt1Pi02GammaGamma:                                 0           0
Hlt1DiElectronHighMass_SS:                          0           0
Hlt1DiElectronHighMass:                             0           0
Hlt1DiElectronLowMass_massSlice1_prompt:            0           0
Hlt1DiElectronLowMass_SS_massSlice1_prompt:         0           0
Hlt1DiElectronLowMass_massSlice2_prompt:            0           0
Hlt1DiElectronLowMass_SS_massSlice2_prompt:         0           0
Hlt1DiElectronLowMass_massSlice3_prompt:            0           0
Hlt1DiElectronLowMass_SS_massSlice3_prompt:         0           0
Hlt1DiElectronLowMass_massSlice4_prompt:            0           0
Hlt1DiElectronLowMass_SS_massSlice4_prompt:         0           0
Hlt1DiElectronLowMass_massSlice1_displaced:         0           0
Hlt1DiElectronLowMass_SS_massSlice1_displaced:      0           0
Hlt1DiElectronLowMass_massSlice2_displaced:         0           0
Hlt1DiElectronLowMass_SS_massSlice2_displaced:      0           0
Hlt1DiElectronLowMass_massSlice3_displaced:         0           0
Hlt1DiElectronLowMass_SS_massSlice3_displaced:      0           0
Hlt1DiElectronLowMass_massSlice4_displaced:         0           0
Hlt1DiElectronLowMass_SS_massSlice4_displaced:      0           0
Hlt1Passthrough:                                    0           0
Hlt1TAEPassthrough:                                 0           0
Hlt1BGIPseudoPVsNoBeam:                             0           0
Hlt1BGIPseudoPVsBeamOne:                            6           0
Hlt1BGIPseudoPVsBeamTwo:                            0           0
Hlt1BGIPseudoPVsUpBeamBeam:                         0           0
Hlt1BGIPseudoPVsDownBeamBeam:                       0           0
Hlt1ODINLumi:                                       0           0
Hlt1ODIN1kHzLumi:                                   0           0
Hlt1ODINCalib:                                      0           0
Hlt1ErrorBank:                                      0           0
Hlt1VeloMicroBiasVeloClosing:                       0           0
Hlt1RICH1Alignment:                                 0           0
Hlt1RICH2Alignment:                                 0           0
Hlt1D2KPiAlignment:                                 0           0
Hlt1Dst2D0PiAlignment:                              0           0
Hlt1MaterialVertexSeedsDownstreamz:                 0           0
Hlt1MaterialVertexSeeds_DWFS:                       0           0
Hlt1DiMuonJpsiMassAlignment:                        0           0
Hlt1OneMuonTrackLine:                               0           0
Hlt1BeamGas:                                        0           0
Hlt1VeloMicroBias:                                  0           0
Hlt1SMOG2BENoBias:                                  0           0
Hlt1SMOG2PassThroughLowMult5:                      22           0
Hlt1SMOG2BELowMultElectrons:                        3           0
Hlt1SMOG2MinimumBias:                               0           0
Hlt1PassthroughPVinSMOG2:                           0           0
Hlt1SMOG2D2Kpi:                                     2           2
Hlt1SMOG2etacTopp:                                  0           0
Hlt1SMOG2KsTopipi:                                  1           1
Hlt1SMOG22BodyGeneric:                              1           1
Hlt1SMOG22BodyGenericPrompt:                        1           1
Hlt1SMOG2SingleTrackVeryHighPt:                     0           0
Hlt1SMOG2SingleTrackHighPt:                         0           0
Hlt1SMOG2DiMuonHighMass:                            0           0
Hlt1SMOG2SingleMuon:                                3           4
Hlt1SMOG2L0Toppi:                                   2           2

Total decisions:      41
Total tracks:         16
Total calos clusters: 0
Total SVs:            6
Total hits:           407
Total stdinfo:        193


velo_validator validation:
TrackChecker output                               :       220/    28926   0.76% ghosts
01_velo                                           :     15372/    15692  97.96% ( 98.57%),       740 (  4.59%) clones, pur  99.56%, hit eff  94.16%
02_long                                           :      9178/     9266  99.05% ( 99.46%),       370 (  3.88%) clones, pur  99.72%, hit eff  95.27%
03_long_P>5GeV                                    :      6110/     6148  99.38% ( 99.65%),       242 (  3.81%) clones, pur  99.82%, hit eff  95.82%
04_long_strange                                   :       382/      384  99.48% ( 99.21%),        26 (  6.37%) clones, pur  99.46%, hit eff  93.21%
05_long_strange_P>5GeV                            :       212/      214  99.07% ( 98.80%),        16 (  7.02%) clones, pur  99.96%, hit eff  93.87%
08_long_electrons                                 :      1196/     1210  98.84% ( 98.97%),        60 (  4.78%) clones, pur  98.09%, hit eff  94.69%

