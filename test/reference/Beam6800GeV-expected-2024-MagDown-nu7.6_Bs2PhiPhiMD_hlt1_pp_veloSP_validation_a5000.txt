long_validator validation:
TrackChecker output                               :     17334/    44915  38.59% ghosts
for P>3GeV,Pt>0.5GeV                              :     12345/    30306  40.73% ghosts
01_long                                           :     26288/    78155  33.64% ( 34.45%),       422 (  1.58%) clones, pur  92.78%, hit eff  98.49%
02_long_P>5GeV                                    :     23889/    51059  46.79% ( 47.84%),       394 (  1.62%) clones, pur  92.95%, hit eff  98.64%
03_long_strange                                   :       774/     3858  20.06% ( 20.20%),        11 (  1.40%) clones, pur  94.02%, hit eff  98.15%
04_long_strange_P>5GeV                            :       649/     1834  35.39% ( 37.10%),        10 (  1.52%) clones, pur  94.27%, hit eff  98.38%
05_long_fromB                                     :      1930/     4585  42.09% ( 40.86%),        29 (  1.48%) clones, pur  92.54%, hit eff  98.90%
06_long_fromB_P>5GeV                              :      1851/     3752  49.33% ( 46.65%),        29 (  1.54%) clones, pur  92.60%, hit eff  98.98%
07_long_electrons                                 :       310/     5658   5.48% (  5.56%),         5 (  1.59%) clones, pur  92.91%, hit eff  97.89%
08_long_electrons_P>5GeV                          :       282/     2953   9.55% ( 10.27%),         3 (  1.05%) clones, pur  92.93%, hit eff  97.93%
09_long_fromB_electrons                           :        40/      240  16.67% ( 20.60%),         1 (  2.44%) clones, pur  92.27%, hit eff  98.57%
10_long_fromB_electrons_P>5GeV                    :        39/      168  23.21% ( 26.43%),         0 (  0.00%) clones, pur  92.34%, hit eff  98.50%
long_P>5GeV_AND_Pt>1GeV                           :      6364/    13336  47.72% ( 48.53%),        88 (  1.36%) clones, pur  92.60%, hit eff  98.95%
long_fromB_P>5GeV_AND_Pt>1GeV                     :      1188/     2500  47.52% ( 45.83%),        15 (  1.25%) clones, pur  92.38%, hit eff  99.18%
11_noVelo_UT                                      :         0/     8699   0.00% (  0.00%),         0 (  0.00%) clones, pur   -nan%, hit eff   -nan%
12_noVelo_UT_P>5GeV                               :         0/     3556   0.00% (  0.00%),         0 (  0.00%) clones, pur   -nan%, hit eff   -nan%
13_long_PT>2GeV                                   :      1446/     3268  44.25% ( 43.74%),        20 (  1.36%) clones, pur  92.69%, hit eff  99.09%
14_long_from_B_PT>2GeV                            :       517/     1172  44.11% ( 43.93%),         2 (  0.39%) clones, pur  92.35%, hit eff  99.41%
15_long_strange_P>5GeV                            :       649/     1834  35.39% ( 37.10%),        10 (  1.52%) clones, pur  94.27%, hit eff  98.38%
16_long_strange_P>5GeV_PT>500MeV                  :       363/      738  49.19% ( 49.86%),         3 (  0.82%) clones, pur  94.17%, hit eff  98.64%
17_long_fromSignal                                :      1214/     2621  46.32% ( 44.75%),        18 (  1.46%) clones, pur  92.53%, hit eff  99.01%


muon_validator validation:
Muon fraction in all MCPs:                                              14960/  1181372   0.01% 
Muon fraction in MCPs to which a track(s) was matched:                    247/    32211   0.01% 
Correctly identified muons with isMuon:                                   219/      247  88.66% 
Correctly identified muons from strange decays with isMuon:                 0/        0   -nan% 
Correctly identified muons from B decays with isMuon:                      46/       49  93.88% 
Tracks identified as muon with isMuon, but matched to non-muon MCP:      2482/    31964   7.76% 
Ghost tracks identified as muon with isMuon:                             1694/    17334   9.77% 


pv_validator validation:
REC and MC vertices matched by dz distance
MC PV is reconstructible if at least 4 tracks are reconstructed
MC PV is isolated if dz to closest reconstructible MC PV > 10.00 mm
REC and MC vertices matched by dz distance

All                  :  0.942 (  5620/  5967)
Isolated             :  0.979 (  2911/  2972)
Close                :  0.905 (  2709/  2995)
False rate           :  0.019 (   107/  5727)
Real false rate      :  0.019 (   107/  5727)
Clones               :  0.000 (     0/  5620)


rate_validator validation:
Hlt1TrackMVA:                                     232/  1000, ( 6960.00 +/-   400.45) kHz
Hlt1TwoTrackMVA:                                  523/  1000, (15690.00 +/-   473.84) kHz
Hlt1D2KK:                                          25/  1000, (  750.00 +/-   148.11) kHz
Hlt1D2KPi:                                         32/  1000, (  960.00 +/-   166.97) kHz
Hlt1D2PiPi:                                        19/  1000, (  570.00 +/-   129.52) kHz
Hlt1Dst2D0Pi:                                       1/  1000, (   30.00 +/-    29.98) kHz
Hlt1KsToPiPi:                                       0/  1000, (    0.00 +/-     0.00) kHz
Hlt1KsToPiPiDoubleMuonMisID:                        0/  1000, (    0.00 +/-     0.00) kHz
Hlt1TwoTrackKs:                                     6/  1000, (  180.00 +/-    73.26) kHz
Hlt1TwoKs:                                          1/  1000, (   30.00 +/-    29.98) kHz
Hlt1LambdaLLDetachedTrack:                          0/  1000, (    0.00 +/-     0.00) kHz
Hlt1XiOmegaLLL:                                     0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SingleHighPtMuon:                               3/  1000, (   90.00 +/-    51.88) kHz
Hlt1SingleHighPtMuonNoMuID:                         8/  1000, (  240.00 +/-    84.51) kHz
Hlt1DiMuonHighMass:                                13/  1000, (  390.00 +/-   107.46) kHz
Hlt1DiMuonDisplaced:                               18/  1000, (  540.00 +/-   126.13) kHz
Hlt1DiMuonSoft:                                     1/  1000, (   30.00 +/-    29.98) kHz
Hlt1TrackMuonMVA:                                  15/  1000, (  450.00 +/-   115.31) kHz
Hlt1DiMuonNoIP:                                     3/  1000, (   90.00 +/-    51.88) kHz
Hlt1DiMuonNoIP_SS:                                  0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDrellYan_VLowMass:                        0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDrellYan_VLowMass_SS:                     0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDrellYan:                                 0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDrellYan_SS:                              0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DetJpsiToMuMuPosTagLine:                        1/  1000, (   30.00 +/-    29.98) kHz
Hlt1DetJpsiToMuMuNegTagLine:                        1/  1000, (   30.00 +/-    29.98) kHz
Hlt1TrackElectronMVA:                              48/  1000, ( 1440.00 +/-   202.80) kHz
Hlt1SingleHighPtElectron:                           7/  1000, (  210.00 +/-    79.09) kHz
Hlt1DiElectronDisplaced:                           10/  1000, (  300.00 +/-    94.39) kHz
Hlt1DiPhotonHighMass:                              13/  1000, (  390.00 +/-   107.46) kHz
Hlt1Pi02GammaGamma:                                 0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronHighMass_SS:                          1/  1000, (   30.00 +/-    29.98) kHz
Hlt1DiElectronHighMass:                             1/  1000, (   30.00 +/-    29.98) kHz
Hlt1DiElectronLowMass_massSlice1_prompt:            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice1_prompt:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice2_prompt:            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice2_prompt:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice3_prompt:            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice3_prompt:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice4_prompt:            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice4_prompt:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice1_displaced:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice1_displaced:      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice2_displaced:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice2_displaced:      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice3_displaced:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice3_displaced:      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice4_displaced:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice4_displaced:      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1Passthrough:                                    0/  1000, (    0.00 +/-     0.00) kHz
Hlt1TAEPassthrough:                                 0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsNoBeam:                             0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsBeamOne:                            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsBeamTwo:                            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsUpBeamBeam:                         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsDownBeamBeam:                       0/  1000, (    0.00 +/-     0.00) kHz
Hlt1ODINLumi:                                       0/  1000, (    0.00 +/-     0.00) kHz
Hlt1ODIN1kHzLumi:                                   0/  1000, (    0.00 +/-     0.00) kHz
Hlt1ODINCalib:                                      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1ErrorBank:                                      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1VeloMicroBiasVeloClosing:                       0/  1000, (    0.00 +/-     0.00) kHz
Hlt1RICH1Alignment:                                 1/  1000, (   30.00 +/-    29.98) kHz
Hlt1RICH2Alignment:                                 0/  1000, (    0.00 +/-     0.00) kHz
Hlt1D2KPiAlignment:                                10/  1000, (  300.00 +/-    94.39) kHz
Hlt1Dst2D0PiAlignment:                              1/  1000, (   30.00 +/-    29.98) kHz
Hlt1MaterialVertexSeedsDownstreamz:                 0/  1000, (    0.00 +/-     0.00) kHz
Hlt1MaterialVertexSeeds_DWFS:                       1/  1000, (   30.00 +/-    29.98) kHz
Hlt1DiMuonJpsiMassAlignment:                        1/  1000, (   30.00 +/-    29.98) kHz
Hlt1OneMuonTrackLine:                               2/  1000, (   60.00 +/-    42.38) kHz
Hlt1BeamGas:                                        0/  1000, (    0.00 +/-     0.00) kHz
Hlt1VeloMicroBias:                                  1/  1000, (   30.00 +/-    29.98) kHz
Hlt1SMOG2BENoBias:                                  0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2PassThroughLowMult5:                       0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2BELowMultElectrons:                        0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2MinimumBias:                               0/  1000, (    0.00 +/-     0.00) kHz
Hlt1PassthroughPVinSMOG2:                           0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2D2Kpi:                                     0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2etacTopp:                                  0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2KsTopipi:                                  0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG22BodyGeneric:                              0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG22BodyGenericPrompt:                        0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2SingleTrackVeryHighPt:                     0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2SingleTrackHighPt:                         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2DiMuonHighMass:                            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2SingleMuon:                                0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2L0Toppi:                                   0/  1000, (    0.00 +/-     0.00) kHz
Inclusive:                                        587/  1000, (17610.00 +/-   467.11) kHz


selreport_validator validation:
                                               Events  Candidates
Hlt1TrackMVA:                                     232         336
Hlt1TwoTrackMVA:                                  523        1614
Hlt1D2KK:                                          25          26
Hlt1D2KPi:                                         32          32
Hlt1D2PiPi:                                        19          19
Hlt1Dst2D0Pi:                                       1           1
Hlt1KsToPiPi:                                       0           0
Hlt1KsToPiPiDoubleMuonMisID:                        0           0
Hlt1TwoTrackKs:                                     6           6
Hlt1TwoKs:                                          1           1
Hlt1LambdaLLDetachedTrack:                          0           0
Hlt1XiOmegaLLL:                                     0           0
Hlt1SingleHighPtMuon:                               3           3
Hlt1SingleHighPtMuonNoMuID:                         8           9
Hlt1DiMuonHighMass:                                13          19
Hlt1DiMuonDisplaced:                               18          22
Hlt1DiMuonSoft:                                     1           1
Hlt1TrackMuonMVA:                                  15          17
Hlt1DiMuonNoIP:                                     3           4
Hlt1DiMuonNoIP_SS:                                  0           0
Hlt1DiMuonDrellYan_VLowMass:                        0           0
Hlt1DiMuonDrellYan_VLowMass_SS:                     0           0
Hlt1DiMuonDrellYan:                                 0           0
Hlt1DiMuonDrellYan_SS:                              0           0
Hlt1DetJpsiToMuMuPosTagLine:                        1           1
Hlt1DetJpsiToMuMuNegTagLine:                        1           1
Hlt1TrackElectronMVA:                              48          48
Hlt1SingleHighPtElectron:                           7          11
Hlt1DiElectronDisplaced:                           10          13
Hlt1DiPhotonHighMass:                              13          20
Hlt1Pi02GammaGamma:                                 0           0
Hlt1DiElectronHighMass_SS:                          1           2
Hlt1DiElectronHighMass:                             1           1
Hlt1DiElectronLowMass_massSlice1_prompt:            0           0
Hlt1DiElectronLowMass_SS_massSlice1_prompt:         0           0
Hlt1DiElectronLowMass_massSlice2_prompt:            0           0
Hlt1DiElectronLowMass_SS_massSlice2_prompt:         0           0
Hlt1DiElectronLowMass_massSlice3_prompt:            0           0
Hlt1DiElectronLowMass_SS_massSlice3_prompt:         0           0
Hlt1DiElectronLowMass_massSlice4_prompt:            0           0
Hlt1DiElectronLowMass_SS_massSlice4_prompt:         0           0
Hlt1DiElectronLowMass_massSlice1_displaced:         0           0
Hlt1DiElectronLowMass_SS_massSlice1_displaced:      0           0
Hlt1DiElectronLowMass_massSlice2_displaced:         0           0
Hlt1DiElectronLowMass_SS_massSlice2_displaced:      0           0
Hlt1DiElectronLowMass_massSlice3_displaced:         0           0
Hlt1DiElectronLowMass_SS_massSlice3_displaced:      0           0
Hlt1DiElectronLowMass_massSlice4_displaced:         0           0
Hlt1DiElectronLowMass_SS_massSlice4_displaced:      0           0
Hlt1Passthrough:                                    0           0
Hlt1TAEPassthrough:                                 0           0
Hlt1BGIPseudoPVsNoBeam:                             0           0
Hlt1BGIPseudoPVsBeamOne:                            0           0
Hlt1BGIPseudoPVsBeamTwo:                            0           0
Hlt1BGIPseudoPVsUpBeamBeam:                         0           0
Hlt1BGIPseudoPVsDownBeamBeam:                       0           0
Hlt1ODINLumi:                                       0           0
Hlt1ODIN1kHzLumi:                                   0           0
Hlt1ODINCalib:                                      0           0
Hlt1ErrorBank:                                      0           0
Hlt1VeloMicroBiasVeloClosing:                       0           0
Hlt1RICH1Alignment:                                 1           2
Hlt1RICH2Alignment:                                 0           0
Hlt1D2KPiAlignment:                                10          10
Hlt1Dst2D0PiAlignment:                              1           1
Hlt1MaterialVertexSeedsDownstreamz:                 0           0
Hlt1MaterialVertexSeeds_DWFS:                       1           0
Hlt1DiMuonJpsiMassAlignment:                        1           1
Hlt1OneMuonTrackLine:                               2           0
Hlt1BeamGas:                                        0           0
Hlt1VeloMicroBias:                                  1           0
Hlt1SMOG2BENoBias:                                  0           0
Hlt1SMOG2PassThroughLowMult5:                       0           0
Hlt1SMOG2BELowMultElectrons:                        0           0
Hlt1SMOG2MinimumBias:                               0           0
Hlt1PassthroughPVinSMOG2:                           0           0
Hlt1SMOG2D2Kpi:                                     0           0
Hlt1SMOG2etacTopp:                                  0           0
Hlt1SMOG2KsTopipi:                                  0           0
Hlt1SMOG22BodyGeneric:                              0           0
Hlt1SMOG22BodyGenericPrompt:                        0           0
Hlt1SMOG2SingleTrackVeryHighPt:                     0           0
Hlt1SMOG2SingleTrackHighPt:                         0           0
Hlt1SMOG2DiMuonHighMass:                            0           0
Hlt1SMOG2SingleMuon:                                0           0
Hlt1SMOG2L0Toppi:                                   0           0

Total decisions:      999
Total tracks:         1940
Total calos clusters: 33
Total SVs:            1724
Total hits:           50743
Total stdinfo:        23547


veloUT_validator validation:
TrackChecker output                               :     35817/    71601  50.02% ghosts
01_velo                                           :     34700/   135811  25.55% ( 25.94%),       544 (  1.54%) clones, pur  87.61%, hit eff  93.70%
02_velo+UT                                        :     34614/   118083  29.31% ( 29.77%),       542 (  1.54%) clones, pur  87.63%, hit eff  93.69%
03_velo+UT_P>5GeV                                 :     30193/    61182  49.35% ( 50.01%),       499 (  1.63%) clones, pur  88.06%, hit eff  94.12%
04_velo+notLong                                   :      5937/    57656  10.30% ( 10.59%),        99 (  1.64%) clones, pur  87.83%, hit eff  93.68%
05_velo+UT+notLong                                :      5862/    40940  14.32% ( 14.76%),        97 (  1.63%) clones, pur  87.93%, hit eff  93.65%
06_velo+UT+notLong_P>5GeV                         :      4901/    10976  44.65% ( 45.35%),        89 (  1.78%) clones, pur  88.64%, hit eff  94.79%
07_long                                           :     28763/    78155  36.80% ( 37.21%),       445 (  1.52%) clones, pur  87.56%, hit eff  93.70%
08_long_P>5GeV                                    :     25303/    51059  49.56% ( 50.13%),       410 (  1.59%) clones, pur  87.95%, hit eff  93.99%
09_long_fromB                                     :      1996/     4585  43.53% ( 42.19%),        30 (  1.48%) clones, pur  86.97%, hit eff  94.25%
10_long_fromB_P>5GeV                              :      1892/     3752  50.43% ( 47.70%),        29 (  1.51%) clones, pur  87.08%, hit eff  94.39%
11_long_electrons                                 :       399/     5658   7.05% (  7.22%),         6 (  1.48%) clones, pur  87.35%, hit eff  92.72%
12_long_fromB_electrons                           :        48/      240  20.00% ( 23.81%),         1 (  2.04%) clones, pur  85.94%, hit eff  94.35%
13_long_fromB_electrons_P>5GeV                    :        44/      168  26.19% ( 29.17%),         0 (  0.00%) clones, pur  86.19%, hit eff  95.42%


velo_validator validation:
TrackChecker output                               :    164746/   316220  52.10% ghosts
01_velo                                           :     76469/   135811  56.31% ( 56.40%),      1950 (  2.49%) clones, pur  84.94%, hit eff  81.48%
02_long                                           :     49686/    78155  63.57% ( 63.56%),      1022 (  2.02%) clones, pur  84.84%, hit eff  82.28%
03_long_P>5GeV                                    :     36610/    51059  71.70% ( 71.59%),       720 (  1.93%) clones, pur  85.44%, hit eff  83.28%
04_long_strange                                   :      2337/     3858  60.58% ( 60.30%),        50 (  2.09%) clones, pur  86.61%, hit eff  83.58%
05_long_strange_P>5GeV                            :      1289/     1834  70.28% ( 70.48%),        25 (  1.90%) clones, pur  87.62%, hit eff  85.20%
06_long_fromB                                     :      2546/     4585  55.53% ( 52.95%),        40 (  1.55%) clones, pur  83.83%, hit eff  81.75%
07_long_fromB_P>5GeV                              :      2213/     3752  58.98% ( 55.33%),        37 (  1.64%) clones, pur  83.92%, hit eff  81.96%
08_long_electrons                                 :      2566/     5658  45.35% ( 46.96%),        57 (  2.17%) clones, pur  84.67%, hit eff  82.37%
09_long_fromB_electrons                           :       108/      240  45.00% ( 47.99%),         2 (  1.82%) clones, pur  83.84%, hit eff  81.22%
10_long_fromB_electrons_P>5GeV                    :        82/      168  48.81% ( 50.95%),         0 (  0.00%) clones, pur  83.47%, hit eff  81.98%
11_long_fromSignal                                :      1418/     2621  54.10% ( 52.59%),        19 (  1.32%) clones, pur  83.45%, hit eff  81.64%

