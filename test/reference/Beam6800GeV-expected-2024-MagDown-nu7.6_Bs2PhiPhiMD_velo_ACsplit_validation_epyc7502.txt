pv_validator_A_side validation:
REC and MC vertices matched by dz distance
MC PV is reconstructible if at least 4 tracks are reconstructed
MC PV is isolated if dz to closest reconstructible MC PV > 10.00 mm
REC and MC vertices matched by dz distance

All                  :  0.865 (  5159/  5967)
Isolated             :  0.909 (  2702/  2972)
Close                :  0.820 (  2457/  2995)
False rate           :  0.008 (    44/  5203)
Real false rate      :  0.008 (    44/  5203)
Clones               :  0.000 (     0/  5159)


pv_validator_C_side validation:
REC and MC vertices matched by dz distance
MC PV is reconstructible if at least 4 tracks are reconstructed
MC PV is isolated if dz to closest reconstructible MC PV > 10.00 mm
REC and MC vertices matched by dz distance

All                  :  0.866 (  5166/  5967)
Isolated             :  0.904 (  2688/  2972)
Close                :  0.827 (  2478/  2995)
False rate           :  0.008 (    40/  5206)
Real false rate      :  0.008 (    40/  5206)
Clones               :  0.000 (     0/  5166)


velo_validator_A_side validation:
TrackChecker output                               :      3280/   155854   2.10% ghosts
01_velo                                           :     66397/   135811  48.89% ( 48.86%),      1648 (  2.42%) clones, pur  99.65%, hit eff  95.26%
02_long                                           :     38711/    78155  49.53% ( 49.49%),       798 (  2.02%) clones, pur  99.77%, hit eff  96.38%
03_long_P>5GeV                                    :     25370/    51059  49.69% ( 49.70%),       504 (  1.95%) clones, pur  99.79%, hit eff  96.89%
04_long_strange                                   :      1888/     3858  48.94% ( 49.25%),        38 (  1.97%) clones, pur  99.44%, hit eff  95.86%
05_long_strange_P>5GeV                            :       875/     1834  47.71% ( 47.23%),        19 (  2.13%) clones, pur  99.30%, hit eff  96.32%
06_long_fromB                                     :      2253/     4585  49.14% ( 49.23%),        41 (  1.79%) clones, pur  99.66%, hit eff  96.87%
07_long_fromB_P>5GeV                              :      1870/     3752  49.84% ( 49.44%),        37 (  1.94%) clones, pur  99.74%, hit eff  96.97%
08_long_electrons                                 :      2725/     5658  48.16% ( 48.24%),       110 (  3.88%) clones, pur  97.77%, hit eff  95.33%
09_long_fromB_electrons                           :       128/      240  53.33% ( 53.11%),         6 (  4.48%) clones, pur  98.40%, hit eff  95.82%
10_long_fromB_electrons_P>5GeV                    :        86/      168  51.19% ( 52.50%),         3 (  3.37%) clones, pur  98.33%, hit eff  96.19%
11_long_fromSignal                                :      1261/     2621  48.11% ( 48.50%),        23 (  1.79%) clones, pur  99.61%, hit eff  96.99%


velo_validator_C_side validation:
TrackChecker output                               :      3496/   158642   2.20% ghosts
01_velo                                           :     66534/   135811  48.99% ( 49.12%),      1886 (  2.76%) clones, pur  99.66%, hit eff  95.59%
02_long                                           :     38350/    78155  49.07% ( 49.17%),       869 (  2.22%) clones, pur  99.77%, hit eff  96.84%
03_long_P>5GeV                                    :     25037/    51059  49.04% ( 49.06%),       542 (  2.12%) clones, pur  99.79%, hit eff  97.36%
04_long_strange                                   :      1848/     3858  47.90% ( 48.25%),        32 (  1.70%) clones, pur  99.28%, hit eff  96.47%
05_long_strange_P>5GeV                            :       892/     1834  48.64% ( 49.05%),        12 (  1.33%) clones, pur  99.04%, hit eff  97.36%
06_long_fromB                                     :      2262/     4585  49.33% ( 49.35%),        45 (  1.95%) clones, pur  99.73%, hit eff  97.10%
07_long_fromB_P>5GeV                              :      1833/     3752  48.85% ( 49.37%),        34 (  1.82%) clones, pur  99.76%, hit eff  97.55%
08_long_electrons                                 :      2725/     5658  48.16% ( 48.47%),       123 (  4.32%) clones, pur  97.77%, hit eff  95.51%
09_long_fromB_electrons                           :       104/      240  43.33% ( 44.14%),         3 (  2.80%) clones, pur  99.31%, hit eff  96.74%
10_long_fromB_electrons_P>5GeV                    :        78/      168  46.43% ( 45.71%),         2 (  2.50%) clones, pur  99.23%, hit eff  97.56%
11_long_fromSignal                                :      1320/     2621  50.36% ( 50.06%),        25 (  1.86%) clones, pur  99.62%, hit eff  97.21%

